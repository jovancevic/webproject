const Welcome = {template: '<welcome></welcome>'}
const Login = {template: '<login></login>'}
const Registration = {template:'<registration></registration>'}
const Feed = {template:'<feed></feed>'}
const Navbar = {template:'<navbar></navbar>'}
const Post = {template:'<post></post>'}
const Settings = {template: '<settings></settings>'}
const Search = {template: '<search></search>'}
const ResultPage = {template:'<resultPage></resultPage>'}
const Profile = {template:'<profile></profile>'}
const AdminFeed = {template:'<adminFeed></adminFeed>'}
const MyProfile = {template:'<myProfile></myProfile>'}
const Chat = {template:'<chat></chat>'}
const PictureView = {template:'<pictureView></pictureView>'}
const Requests = {template:'<requests></requests>'}
const Friends = {template:'<friends></friends>'}



const router = new VueRouter({
	mode:'hash',
	routes:[
		{path:'/', component:Welcome},
		{path:'/login', name:'login', component:Login},
		{path:'/registration', component:Registration},
		{path:'/navbar', component: Navbar},
		{path:'/feed', component:Feed},
		{path:'/resultPage', name:"resultPage", component: ResultPage, props:true},
		{path:'/post',name:'post', component: Post, props:true},
		{path:'/settings', component: Settings},
		{path:'/search', component: Search},
		{path:'/adminFeed', name:"adminFeed" ,component: AdminFeed},
		{path:'/chat', name:"chat" ,component: Chat, props:true},
		{path:'/requests', name:"requests" ,component: Requests, props:true},
		{path:'/profile', name:"profile", component: Profile, props:true},
		{path:'/friends', name:"friends", component: Friends, props:true},
		{path:'/myProfile', name:"myProfile", component: MyProfile, props:true},
		{path:'/pictureView', name:"pictureView", component: PictureView, props:true},
	]
});

var app = new Vue({
	router,
	el: '#welcome'
});