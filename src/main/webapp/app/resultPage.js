Vue.component("resultPage",{
	data(){
        return{
            results: null,
			ascName: 1,
			ascSurname: 1,
			ascDate:true,
			loggedUser:null
        }
    },
	template:
	`	
		<div>
			<navbar v-if="loggedUser"></navbar>
			<search></search>
			<br> 
			<div class="container"><h1>Search results</h1></div>
			<ul class="nav nav-pills justify-content-center">
			  <li class="nav-item" >
			    <p class="nav-link">Sort by</p>
			  </li>
			  <li class="nav-item" v-on:click="sortBy('name')">
			    <a class="nav-link active" id="name" href="#">Ime</a>
			  </li>
			  <li class="nav-item" v-on:click="sortBy('surname')">
			    <a class="nav-link" id="surname" href="#">Prezime</a>
			  </li>
			  <li class="nav-item" v-on:click="sortBy('date')">
			    <a class="nav-link" id="date" href="#">Datum rodjenja</a>
			  </li>
			</ul>
				<div class="d-flex align-items-center justify-content-center">
					<div v-if="!results || results.length == 0" class="container"><h1>No results.</h1></div>
					<table border='1'>
						<tr>
							<td>
								<div v-for="user in results" v-on:click="selectedUser(user)" class="card" style="width: 18rem;">
								  <img v-bind:src="user.profilePicture" class="card-img-top" alt="...">
								  <div class="card-body">
								    <h5 class="card-title">{{user.name}} {{user.surname}}</h5>
								    <p class="card-text">{{user.dateOfBirth}}</p>
								  </div>
								</div>
							</td>
						</tr>
					</table>
				</div>
		</div>
	`
	,
	methods:{
		selectedUser:function(user){
			this.$router.push({name:'profile', params:{user:user}});
		},
		sortBy:function(param){
			let ascNameLocal = this.ascName;
			let ascSurnameLocal = this.ascSurname;
			let ascDateLocal = this.ascDate;
			if(param == "name"){
				$("#name").addClass("active");
				$("#surname").removeClass("active");
				$("#date").removeClass("active");
				this.results.sort(function(a, b){
				    if(a.name < b.name) { return -(ascNameLocal); }
				    if(a.name > b.name) { return ascNameLocal; }
				    return 0;
				});
				this.ascName = -this.ascName;
			}
			else if(param == "surname"){
				$("#name").removeClass("active");
				$("#surname").addClass("active");
				$("#date").removeClass("active");
				this.results.sort(function(a, b){
				    if(a.surname < b.surname) { return -ascSurnameLocal; }
				    if(a.surname > b.surname) { return ascSurnameLocal; }
				    return 0;
				});
				this.ascSurname = -this.ascSurname;
			}
			else{
				$("#name").removeClass("active");
				$("#surname").removeClass("active");
				$("#date").addClass("active");
				this.results.sort(function(a, b){
					let dateA = new Date(b.dateOfBirth);
					let dateB = new Date(a.dateOfBirth);
				    return (ascDateLocal?(dateA - dateB):(dateB - dateA));
				});
				this.ascDate = !this.ascDate;
			}
		}
		
	},
	mounted(){
		
		this.loggedUser = JSON.parse(window.localStorage.getItem("loggedUser"));
		document.removeEventListener("backbutton", () => { this.$router.push('/feed') });
		
		this.$root.$on('form', (value) =>{
			console.log(value.firstName);
			console.log(value.lastName);
			console.log(value.date1);
			console.log(value.date2);
			
			axios.get('rest/user/search', {params:
			 {
				firstName: value.firstName,
				lastName: value.lastName,
				date1: value.date1,
				date2: value.date2}
				}).then((res) => {
					this.results = res.data;
                 });
		});
		
		axios.get('rest/user/search', {params:
			 {
				firstName: this.$route.params.firstName,
				lastName: this.$route.params.lastName,
				date1: this.$route.params.date1,
				date2: this.$route.params.date2}
				}).then((res) => {
					this.results = res.data;
                 });
	}
})