Vue.component("welcome",{
	data(){
        return{
            searchInput: '',
			loggedUser : null,
        }
    },
	template:
	`	
		<div class="align-items-center justify-content-center">
			<search></search>
		 	<div class="d-grid gap-2 col-4 mx-auto">
				  <button @click="$router.push('login')" class="btn btn-primary" type="button">Login</button>
				  <button @click="$router.push('registration')" class="btn btn-primary" type="button">Register</button>	
			</div>
			<link rel="stylesheet" href="css/welcome.css" type="text/css">
		</div>
	`
	,
	methods:{
		
	}
	,
	mounted(){
		window.localStorage.clear();
		console.log("clear");
		this.loggedUser = JSON.parse(window.localStorage.getItem("loggedUser"));
	}
})