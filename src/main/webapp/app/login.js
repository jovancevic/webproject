Vue.component("login",{
	data(){
        return{
            form: {
                username: '',
                password: ''
            },
			loginError:false,
			blockedError: false
        }
    },
	template:
	`	
		<div class="loginComponent d-flex align-items-center justify-content-center">
		 	<div class="row">
			    
			    <div class="col">
			      <form v-on:submit.prevent="checkForm" class="form-floating">
						<h1>Login Here</h1>
						<hr>
						<div class="form-floating mb-3">
							 <input type="text" class="form-control" id="floatingInput" placeholder="username" v-model="form.username" required>
							 <label for="floatingInput">Username</label>
						</div>
						<div class="form-floating">
							 <input type="password" class="form-control" id="floatingPassword" placeholder="Password" v-model="form.password"  required>
							 <label for="floatingPassword">Password</label>
							<br>
						</div>
						<button type="submit" class="btn btn-primary"><strong>Log in</strong></button>
						<p>Don't have an account? <a href="#/registration">Register</a>.</p>
					</form>
					<p v-if="loginError" class="error" style="color:red">
					    <b>Wrong credentials!</b>
					</p>
					<p v-if="blockedError" class="error" style="color:red">
					    <b>You are blocked!</b>
					</p>
			    </div>
			   
		 	</div>
			<link rel="stylesheet" href="css/login.css" type="text/css">
		</div>
		
	`
	,
	methods:{
		checkForm : function(){
			
			axios.post('rest/log/in', this.form)
             .then((res) => {
                
				console.log("Ulogovan: " + res.data)
				if(res.status !== 204){
					if(!res.data.blocked){
					window.localStorage.setItem('loggedUser', JSON.stringify(res.data));
					this.blockedError = false;	
					this.loginError = false;
						if(res.data.role == 'USER'){
							this.$router.push('/feed');
						}else{
							this.$router.push('/adminFeed');	
						}
					}
						this.blockedError = true;
						return;
				}
				
				 this.loginError = true;
				
             });
		}
	}
	,
	mounted(){
		window.localStorage.clear();
		document.removeEventListener("backbutton", () => { $router.go(-1) });
		
	}
})