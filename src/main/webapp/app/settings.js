
Vue.component("settings",{
	data(){
        return{
            form: {
                username: '',
                email: '',
                name: '',
                surname: '',
                gender: '',
				oldPassword: '',
                password: '',
                confirm: '',
				dateOfBirth: '',
				private: '',
				passwordChange: false
            },
			submitError: false,
			passwordError: false, 
			successfulUpdate: false
        }
    },
	template:
	`	<div>
			<navbar></navbar>
			
			<div class="container">
		 	<div class="row">
			    <div class="col"></div>
			    <div class="col">
					<form v-on:submit.prevent="checkForm">
						<h1>Edit personal informations:</h1>
						<hr>
						<label for="username"><b>Username</b></label>
						<input type="text" class="form-control" id="floatingInput" placeholder="Enter Username" name="username" v-model="form.username" readonly>
						<br>
						<label for="email"><b>Email</b></label>
						<input type="email" class="form-control" id="floatingInput" placeholder="Enter Email" name="email" v-model="form.email" required>
						<br>
						<label for="name"><b>First Name</b></label>
						<input type="text" class="form-control" id="floatingInput" placeholder="Enter Name" name="name" v-model="form.name" required>
						<br>
						<label for="surname"><b>Last Name</b></label>
						<input type="text" class="form-control" id="floatingInput" placeholder="Enter Last Name" name="surname" v-model="form.surname" required>
						<br>
						<label for="gender"><b>Gender</b>&nbsp;&nbsp;&nbsp;</label>
						<select name="gender" v-model="form.gender" class="btn btn-primary">
							<option value="MALE">MALE</option>
							<option value="FEMALE">FEMALE</option>
						</select>
						<br>
						<label for="date"><b>Date of birth</b></label>
						<input type="date" class="form-control" id="floatingInput" placeholder="date" name="confirm" v-model="form.dateOfBirth" required>
						<br>
						<div class="form-check form-switch">
						  <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked" name="privateAccount" v-model="form.private" checked>
						  <label class="form-check-label" for="privateAccount">Private account</label>
						</div>
						<br>
						<button v-on:click="showPasswordInputs()" type="button">Change password</button>
						<div v-if="form.passwordChange">
							<br>
								<label for="oldPassword"><b>Old Password</b></label>
								<input type="password" class="form-control" id="floatingInput" placeholder="Enter Old Password" name="oldPassword"  v-model="form.oldPassword" required>
							<br>
								<label for="password"><b>Password</b></label>
								<input type="password" class="form-control" id="floatingInput" placeholder="Enter Password" name="password"  v-model="form.password" required>
							<br>
								<label for="confirm"><b>Confirm Password</b></label>
								<input type="password" class="form-control" id="floatingInput" placeholder="Confirm Password" name="confirm" v-model="form.confirm" required>
							<br>
						</div>
						<hr>
						<button type="submit" class="btn-lg btn-primary"><strong>Confirm changes</strong></button>
					</form>
					<p v-if="submitError"  style="color:red">
					    <b>Old password is not correct!</b>
				  	</p>
					<p v-if="passwordError" style="color:red">
					    <b>Passwords don't match!</b>
				  	</p>
					<p v-if="successfulUpdate">
					    <b>You have successfully updated personal information.</b>
				  	</p>
			    </div>
			    <div class="col"></div>
		 	</div>
		</div>
			
		</div>		
	`
	,
	methods:{
		showPasswordInputs : function(){
			this.form.passwordChange = !this.form.passwordChange;
		},
		
		checkForm : function(){
			if (this.form.password != this.form.confirm){
				this.passwordError = true;
				this.successfulUpdate = false;
				this.submitError = false;
			}
			else{
				console.log(this.form);
				axios.post('rest/user/settings', this.form)
                 .then((res) => {
                     //Perform Success Action
					 this.successfulUpdate = true;
					 this.submitError = false;
					 this.passwordError = false;
					
					window.localStorage.setItem("loggedUser", JSON.stringify(res.data));
				
                 })
                 .catch((error) => {
					 console.log("Stara sifra nije dobra!");
                     this.submitError = true;
					 this.successfulUpdate = false;
					 this.passwordError = false;
                 });
			}
		}
	}
	,
	mounted(){
		this.loggedUser = JSON.parse(window.localStorage.getItem('loggedUser'));
		
		if(!this.loggedUser){
			this.$router.push({name:'login'});
			return;
		}
		
		document.removeEventListener("backbutton", () => { $router.go(-1) });
		
		this.form.username = this.loggedUser.username;
		this.form.email = this.loggedUser.email;
		this.form.name = this.loggedUser.name;
		this.form.surname = this.loggedUser.surname;
		this.form.gender = this.loggedUser.gender;
		this.form.dateOfBirth = this.loggedUser.dateOfBirth;
		this.form.private = this.loggedUser.privateAccount;
		}
})