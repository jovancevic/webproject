
Vue.component("pictureView",{
	data(){
        return{
            user:null,
			picture:null,
			allUsers:null,
			loggedUser:null,
			commentInput:null
        }
    },
	template:
	`	<div>
			<navbar></navbar>
			
			<div class="page-content page-container" id="page-content">
			    <div class="padding">
			        <div class="container d-flex justify-content-center">
			            <div class="col-md-6">
			                <div v-if="allUsers" class="box box-widget">
			                    <div class="box-header with-border">
			                        <div class="user-block"> <img class="img-circle" v-bind:src="user.profilePicture" alt="User Image"> <span class="username" v-on:click="goToProfile(user.username)"><a href="#" data-abc="true">{{user.name}} {{user.surname}}</a></span></div>
			                        <div class="box-tools"><button v-on:click="removePicture()" v-if="hasPermission()" type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> </div>
			                    </div>
			                    <div class="box-body"> <img class="img-responsive pad picPic" v-bind:src="picture.picture" alt="Photo">
			                        <p>{{picture.text}}</p>
			                    </div>
			                    <div class="box-footer box-comments">
			                        <div v-for="comment in picture.comments" v-if="!comment.deleted" class="box-comment"> <img class="img-circle img-sm" v-bind:src="getCommentProfilePicture(comment.publisher)" alt="User Image">
			                            <div class="comment-text"> <span v-on:click="goToProfile(comment.publisher)" class="username"><a href="#"> {{getName(comment.publisher)}} </a><span v-if="comment.editDate" class="text-muted pull-right">Edited:{{comment.editDate}}</span><span class="text-muted pull-right" v-if="comment.editDate == null">{{comment.date}}</span>
												 </span> {{comment.text}}</div><span v-if="comment.publisher == loggedUser.username"><a href="#" v-on:click="removeComment(comment.id)">remove</a></span>
			                        </div>
			                    </div>
			                    <div class="box-footer">
			                        <form action="#" method="post"> <img class="img-responsive img-circle img-sm" src="https://img.icons8.com/color/36/000000/administrator-male.png" alt="Alt Text">
			                            <div class="img-push"> <input v-model="commentInput" type="text" class="form-control input-sm">
											<button v-on:click="comment()" type="button" class="btn btn-outline-primary">Comment</button>
										 </div>
			                        </form>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
			<link rel="stylesheet" href="css/post.css" type="text/css">
		</div>
	`
	,
	methods:{
		goToProfile:function(username){
			var user = null
			console.log(username);
			console.log(this.allUsers);
			if(username == this.user.username){
				user = this.user;
			}
			else{
				for(u of this.allUsers){
					if(u.username == username)	
						user = u;
				}
			}
			console.log(user);
			
			this.$router.push({name:'profile', params:{user:user}});
		},
		
		
		removeComment:function(id){
			axios.delete('rest/feed/deleteCommentPicture/' + this.picture.id + '/' + id)
			.then((res) => {
				this.picture = res.data;
			});
		},
		
		comment:function(post){
			axios.post('rest/feed/commentPicture', {
				picture: this.picture.id,
				username: this.loggedUser.username,
				comment: this.commentInput				
			})
			.then((res) => {
				this.commentInput = "";
				this.picture = res.data;
			});
		},
		
		getName:function(username){
			for(user of this.allUsers){
				if(user.username == username)	
					return user.name+" "+user.surname;
			}
		},
		hasPermission:function(){
			return this.loggedUser.role == 'ADMIN' || this.loggedUser.username == this.user.username;
		},
		removePicture:function(){
			if(this.loggedUser.role == 'ADMIN'){
				
				let reason = prompt("Reason for removing this post:");
				console.log(reason);
				var message = "Your picture #"+this.picture.id+" has been removed by administrator due to: \n'"+reason+"'";
				
				var host = "ws://localhost:8080/SocialMedia/chat";
				console.log(host);
				this.connection = new WebSocket(host);
				// Po uspešnom otvaranju konekcije, ovo se poziva
				var self = this;
				this.connection.onopen = function () { 	
					console.log("Otvorena konekcija.");
				};
				// Greška pri komunikaciji 
				this.connection.onerror = function (error) {
					console.log('WebSocket Error ' + error); 
				};
				// Pri gašenju se ovo poziva
				this.connection.onclose = function(){
					self.connection = null;
				};
			}
			axios.delete('rest/feed/deletePicture/' + this.picture.id)
			.then((res) => {
				this.user = res.data;
				console.log(this.user.username);
				if(this.user.username == this.loggedUser.username){
					window.localStorage.setItem("loggedUser",JSON.stringify(this.user));					
					this.$router.push({name:'myProfile'});
				}
				else
					this.$router.push({name:'profile', params:{user:this.user}});
					
				if(this.loggedUser.role == 'ADMIN'){
						axios.post('rest/dm/sendMessage', {
						fromUser: this.loggedUser.username,
						toUser: this.user.username,
						content:message})
						.then((res) => {
							this.connection.send(message);
		                 });
					}
			});
		},
		getCommentProfilePicture:function(username){
			for(user of this.allUsers){
				if(user.username == username)	
					return user.profilePicture;
			}
			
		}
		
	}
	,
	mounted(){
			document.removeEventListener("backbutton", () => { $router.go(-1) });
			this.picture = this.$route.params.picture;
			this.user = this.$route.params.user;
			this.loggedUser = loggedUser = JSON.parse(window.localStorage.getItem("loggedUser"));
			console.log(JSON.parse(window.localStorage.getItem("loggedUser")));
			
			axios.get('rest/feed/getPicture', {params:{pictureId: this.picture.id}})
			.then((res) => {
				this.picture = res.data;
			});
			
			
			axios.get('rest/user')
                 .then((res) => {
                     //Perform Success Action
					this.allUsers = res.data;
                 })
                 .catch((error) => {
                     this.submitError = true;
                 });
	}
})