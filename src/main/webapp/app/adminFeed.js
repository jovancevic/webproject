
Vue.component("adminFeed",{
	data(){
        return{
			users:null,
            input: "",
        }
    },
	template:
	`	<div>
			<navbar />
			
			  <nav class="navbar navbar-expand-lg navbar-light bg-light">
				<div class="container-fluid">
				    <a class="navbar-brand" href="#">Admin</a>
					<form class="form-inline" v-on:submit="search()">
						<div class="input-group">
						  <input v-model="input" type="text" class="form-control" placeholder="name or email" aria-label="Username" aria-describedby="basic-addon1">
						  <input type="submit" value="Search">
						</div>
					</form>
				</div>
			  </nav>
			
	
			<div class="d-flex align-items-center justify-content-center">
				<table border='1'>
					<tr>
						<td>
							<div v-for="user in users" v-on:click="selectUser(user)" class="card" style="width: 18rem;">
							  <img v-bind:src="user.profilePicture" class="card-img-top" alt="...">
							  <div class="card-body">
							    <h5 class="card-title">{{user.name}} {{user.surname}}</h5>
							    <p class="card-text">{{user.dateOfBirth}}</p>
								<a v-if="user.blocked" v-on:click.stop="toggleBlockedValue(user.username)" href="#" class="btn btn-primary">Unblock user</a>
								<a v-if="!user.blocked" v-on:click.stop="toggleBlockedValue(user.username)" href="#" class="btn btn-primary">Block user</a>
							  </div>
							</div>
						</td>
					</tr>
				</table>
			</div>
			
			
		</div>
	`
	,
	methods:{
		selectUser: function(user){
			this.$router.push({name:'profile', params:{user:user}});
		},
		search:function(){
			axios.get('rest/user/searchAdmin',  { params: { input: this.input } })
				.then((res) => {
					this.users = res.data;
                 });
		},
		toggleBlockedValue:function(username){
			axios.get('rest/user/toggleBlock',{ params: { username: username} })
				.then((res) => {
					for(user of this.users)
						if(user.username == res.data.username)
							user.blocked = !user.blocked;	
                 });
		}
	}
	,
	mounted(){
			axios.get('rest/user', {withCredentials:true})
                 .then((res) => {
					this.users = res.data;
                 })
                 .catch((error) => {
                     this.submitError = true;
                 });
	}
})