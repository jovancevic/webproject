
Vue.component("chat",{
	data(){
        return{
            user:null,
			loggedUser:null,
			messages: null,
			messageToSend:null,
			connection:null,
			messagedUsers:null			
        }
    },
	template:
	`	
		<div class="container" >
			<navbar></navbar>
			<div class="row justify-content-center">
				<div class="col-md-2 col-sm-2">
					<div v-if="messagedUsers">
			            <div v-for="mUser in messagedUsers" v-on:click="selectedUser(mUser)" class="chat_list active_chat">
				              <div class="chat_people">
				                <div class="chat_img"> <img v-bind:src="mUser.profilePicture" alt="sunil"> 
								</div>
				                <div class="chat_ib">
				                  <h5>{{mUser.name}} {{mUser.surname}}</h5>
				                </div>
				              </div>
						</div>
					</div>
				</div>
			    <div  class="col-md-6 col-sm-6" id="chat_window_1">
		        	<div v-if="user && loggedUser" class="panel panel-default">
		                <div class="panel-heading top-bar">
		                    <div class="col-md-8 col-xs-8">
		                        <h3 class="panel-title"><span class="glyphicon glyphicon-comment"></span> {{user.name}} {{user.surname}} </h3>
		                    </div>
		                    <div class="col-md-4 col-xs-4" style="text-align: right;">
		                        <a href="#"><span id="minim_chat_window" class="glyphicon glyphicon-minus icon_minim"></span></a>
		                        <a href="#"><span class="glyphicon glyphicon-remove icon_close" data-id="chat_window_1"></span></a>
		                    </div>
		                </div>
		                <div v-if="messages" class="panel-body msg_container_base">
							<div v-for="message in messages">
			                    <div v-if="message.sender == loggedUser.username" class="row msg_container base_sent">
			                        <div class="col-md-10 col-xs-10">
			                            <div class="messages msg_sent">
			                                <p>{{message.content}}</p>
			                                <time v-bind:datetime="message.dateTime"></time>
			                            </div>
			                        </div>
			                        <div class="col-md-1 col-xs-2 avatar">
			                            <img v-bind:src="loggedUser.profilePicture" class=" img-responsive ">
			                        </div>
			                    </div>
			                    <div v-if="message.sender == user.username" class="row msg_container base_receive">
			                        <div class="col-md-1 col-xs-2 avatar">
			                            <img v-bind:src="user.profilePicture" class=" img-responsive ">
			                        </div>
			                        <div class="col-md-10 col-xs-10">
			                            <div class="messages msg_receive">
			                                	<div style="color:red" v-if="user.role == 'ADMIN'"><p>{{message.content}}</p></div>
												<div v-else><p>{{message.content}}</p></div>
			                                <time v-bind:datetime="message.dateTime"></time>
			                            </div>
			                        </div>
			                    </div>
							</div>
		                </div>
		                <div class="panel-footer">
		                    <div class="input-group">
		                        <input v-model="messageToSend" id="btn-input" type="text" class="form-control input-sm chat_input" placeholder="Write your message here..." />
		                        <span class="input-group-btn">
		                        <button v-on:click="sendMessage()" class="btn btn-primary btn-sm" id="btn-chat">Send</button>
		                        </span>
		                    </div>
		                </div>
			        </div>
					<div v-else-if="!user">
						Select user to display messages
					</div>
				</div>
		    </div>
			<link rel="stylesheet" href="css/chat.css" type="text/css"/>
		</div>
		
	`
	,
	methods:{
		sendMessage:function(){
			console.log("Saljem poruku: " + this.messageToSend);
			
			
			axios.post('rest/dm/sendMessage', {
				fromUser: this.loggedUser.username,
				toUser: this.user.username,
				content:this.messageToSend}
				).then((res) => {
					this.messages = res.data;
					this.connection.send(this.messageToSend);
					this.messageToSend="";
					
					
					if(this.messagedUsers.length == 0)
						this.messagedUsers.push(this.user);
					else if (this.messagedUsers.filter(e => e.username == this.user.username).length == 0) {
						
						this.messagedUsers.push(this.user);
					}
                 });
			
			
			
			
		},
		selectedUser:function(user){
			console.log("selektovan user"+user);
			axios.get('rest/dm/getMessages', {params:
				 {
					username1: this.loggedUser.username,
					username2: user.username}
					}).then((res) => {
						this.messages = res.data;
						this.user = user;
	                 });
			
			var host = "ws://localhost:8080/SocialMedia/chat";
			console.log(host);
			this.connection = new WebSocket(host);
			
		}
	}
	,
	mounted(){
		this.loggedUser = JSON.parse(window.localStorage.getItem('loggedUser'));
		
		if(!this.loggedUser){
			this.$router.push({name:'login'});
			return;
		}
		
		this.user = this.$route.params.user;
		console.log(this.user);
		
		document.removeEventListener("backbutton", () => { $router.go(-1) });
		
		axios.get('rest/dm/getMessagedUsers', {params:
			 {
				loggedUser: this.loggedUser.username}
				}).then((res) => {
					this.messagedUsers = res.data;
					console.log(this.messagedUsers);
					
					if(this.user == null)
						this.user = this.messagedUsers[0];
					else if (this.messagedUsers.filter(e => e.username == this.user.username ).length < 0) {
						this.messagedUsers.push(this.user);
					}
					
					var self = this;
					axios.get('rest/dm/getMessages', {params:
						 {
							username1: this.loggedUser.username,
							username2: this.user.username}
							}).then((res) => {
								this.messages = res.data;
			                 });
			
					var host = "ws://localhost:8080/SocialMedia/chat";
					console.log(host);
					this.connection = new WebSocket(host);
					// Po uspešnom otvaranju konekcije, ovo se poziva
					this.connection.onopen = function () { 	
						console.log("Otvorena konekcija.")
					};
					// Greška pri komunikaciji 
					this.connection.onerror = function (error) {
						console.log('WebSocket Error ' + error); 
					}; 
					// Poruke od servera
					this.connection.onmessage = function (e) { 	
						
						axios.get('rest/dm/getMessagedUsers', {params:
						 {
							loggedUser: self.loggedUser.username}
							}).then((res) => {
								self.messagedUsers = res.data;
								console.log(self.messagedUsers);
							});
						
						if(self.user != null){
							axios.get('rest/dm/getMessages', {params:
							 {
								username1: self.loggedUser.username,
								username2: self.user.username}
								}).then((res) => {
									self.messages = res.data;
				                 });
						}
					};
					// Pri gašenju se ovo poziva
					this.connection.onclose = function(){
						self.connection = null;
					};	
						
                 });

	}
})