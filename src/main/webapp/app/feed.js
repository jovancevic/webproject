
Vue.component("feed",{
	data(){
        return{
            friends:null,
			loggedUser:null
        }
    },
	template:
	`	<div>
			<navbar> </navbar>
			
			<div class="d-flex align-items-center justify-content-center">
				<div v-if="!friends || friends.length == 0" class="container"> <h1>No posts to show.</h1> </div>
				<table border='1'>
					<tr v-for="friend in friends">
						<td>
							<div v-for="post in friend.posts" v-if="!post.deleted" v-on:click="selectPosts(post, friend)" class="card" style="width: 18rem;">
							  <img v-if="post.picture" v-bind:src="post.picture" class="card-img-top" alt="...">
							  <div class="card-body">
							    <h5 class="card-title">{{friend.name}} {{friend.surname}}</h5>
							    <p class="card-text">{{post.text}}</p>
							    <a href="#" class="btn btn-primary">See post</a>
							  </div>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	`
	,
	methods:{
		selectPosts: function(post, user){
			console.log(post.text);
			this.$router.push({name:'post', params:{post:post, user:user}});
		}
	}
	,
	mounted(){
			this.loggedUser = JSON.parse(window.localStorage.getItem('loggedUser'));
		
			if(this.loggedUser.role == 'ADMIN'){
				this.$router.push({name:'adminFeed'});			
			}
			
			if(!this.loggedUser){
				this.$router.push({name:'login'});
				return;
			}
			document.removeEventListener("backbutton", () => { $router.go(-1) });
			
			axios.get('rest/feed', {params:{username:this.loggedUser.username}})
                 .then((res) => {
                     //Perform Success Action
					console.log("Uspesno dobijeni postovi!");
					this.friends = res.data;
					console.log(this.friends);
                 })
                 .catch((error) => {
                     this.submitError = true;
                 });
	}
})