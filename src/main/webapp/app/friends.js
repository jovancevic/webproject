Vue.component("friends",{
	data(){
        return{
			loggedUser:null,
			friends:null
        }
    },
	template:
	`	
		<div>
			<navbar />
			<div v-if="friends && friends.length > 0" class="container">
				<h1>Friends</h1><br>
				<div class="row">
					<div class="col-md-8">
						<div class="people-nearby">
						  
						  <div v-for="user in friends" class="nearby-user">
							<div class="row">
							  <div class="col-md-2 col-sm-2">
								<img v-bind:src="user.profilePicture" alt="user" class="profile-photo-lg">
							  </div>
							  <div class="col-md-7 col-sm-7">
								<h5><a v-on:click="selectUser(user)" href="#" class="profile-link">{{user.name}} {{user.surname}}</a></h5>
							  </div>
							  <div class="col-md-3 col-sm-3">
								<button v-on:click="unfriend(user)" class="btn btn-primary pull-right">Unfriend</button>
							  </div>
							</div>
						  </div>
						</div>
					</div>
				</div>
			</div>
			<div v-else class="container"><br><h1>No friends</h1></div>
			<link rel="stylesheet" href="css/request.css" type="text/css"/>
		</div>
		
	`
	,
	methods:{
		
		selectUser:function(u){
			this.$router.push({name:'profile', params:{user:u}});
		},

		unfriend:function(u){
			var data={
				"username1": u.username,
				"username2": this.loggedUser.username
			}
			axios.post('rest/user/unfriend', data)
			.then((res) => {
				this.friends = this.friends.filter(
					function(ele){ 
						return ele.username != u.username; 
					});
				this.loggedUser.friends = this.friends;
				window.localStorage.setItem('loggedUser', JSON.stringify(this.loggedUser));
				
			});
		}
		
	},
	
	mounted(){
		this.loggedUser = JSON.parse(window.localStorage.getItem("loggedUser"));
		
		if(!this.loggedUser){
			this.$router.push({name:'login'});
			return;
		}
		document.removeEventListener("backbutton", () => { $router.go(-1) });
		
		axios.get('rest/user/getFriends', {params:{username: this.loggedUser.username}
		}).then((res) => {
					this.loggedUser.friends = [];
					for(u1 of res.data)
						this.loggedUser.friends.push(u1.username);
						
					this.friends = res.data;
					window.localStorage.setItem("loggedUser", JSON.stringify(this.loggedUser));
                
		});
	}
})