Vue.component("registration",{
	data(){
        return{
            form: {
                username: '',
                email: '',
                name: '',
                lastName: '',
                gender: '',
                password: '',
                confirm: '',
				dateOfBirth: ''
            },
			passwordError: false,
			submitError:false
        }
    },
	template:
	`	
		<div class="container">
		 	<div class="row">
			    <div class="col"></div>
			    <div class="col">
					<form v-on:submit.prevent="checkForm">
						<h1>Register Here</h1>
						<p>Please fill in the details to create an account with us.</p>
						<hr>
						<label for="username"><b>Username</b></label>
						<input type="text" class="form-control" id="floatingInput" placeholder="Enter Username" name="username" v-model="form.username" required>
						<br>
						<label for="email"><b>Email</b></label>
						<input type="email" class="form-control" id="floatingInput" placeholder="Enter Email" name="email" v-model="form.email" required>
						<br>
						<label for="name"><b>Name</b></label>
						<input type="text" class="form-control" id="floatingInput" placeholder="Enter Name" name="name" v-model="form.name" required>
						<br>
						<label for="lastname"><b>Last Name</b></label>
						<input type="text" class="form-control" id="floatingInput" placeholder="Enter Last Name" name="lastname" v-model="form.lastName" required>
						<br>
						<label for="gender"><b>Gender</b>&nbsp;&nbsp;&nbsp;</label>
						<select name="gender" v-model="form.gender" class="btn btn-primary" required>
							<option value="MALE">MALE</option>
							<option value="FEMALE">FEMALE</option>
						</select>
						<br><br>
						<label for="password"><b>Password</b></label>
						<input type="password" class="form-control" id="floatingInput" placeholder="Enter Password" name="password"  v-model="form.password" required>
						<br>
						<label for="confirm"><b>Confirm Password</b></label>
						<input type="password" class="form-control" id="floatingInput" placeholder="Confirm Password" name="confirm" v-model="form.confirm" required>
						<br>
						<label for="date"><b>Date of birth</b></label>
						<input type="date" class="form-control" id="floatingInput" placeholder="date" name="confirm" v-model="form.dateOfBirth" required>
						<hr>
						<button type="submit" class="btn-lg btn-primary"><strong>Register</strong></button>
						<p>Already have an account? <a href="#/">Sign in</a>.</p>
					</form>
					<p v-if="passwordError">
					    <b>Password don't match!</b>
				  	</p>
					<p v-if="submitError">
					    <b>Username already exists!</b>
					</p>
			    </div>
			    <div class="col"></div>
		 	</div>
		</div>
	`
	,
	methods:{
		checkForm : function(){
			if (this.form.password != this.form.confirm){
				this.passwordError = true;
			}
			else{
				axios.post('rest/registration', this.form)
                 .then((res) => {
                     //Perform Success Action
					window.localStorage.setItem('loggedUser', JSON.stringify(res.data));
					this.$router.push('/feed');
                 })
                 .catch((error) => {
                     this.submitError = true;
                 });
			}
		}
		
	}
	,
	mounted(){
		document.removeEventListener("backbutton", () => { $router.go(-1) });
	}
})