Vue.component("profile",{
	data(){
        return{
            user: null,
			loggedUser: null,
			mutualFriendsUsername: null,
			mutualFriends: null,
			isFriend: null,
			connection:null
        }
    },
	template:
	`	
		<div v-if="user" class="row py-5 px-4">
			<navbar v-if="loggedUser" />
			    <div class="col-md-5 mx-auto">
			        <!-- Profile widget -->
			        <div class="bg-white shadow rounded overflow-hidden">
			            <div class="px-4 pt-0 pb-5 cover">
			                <div class="media align-items-end profile-head">
			                    <div class="profile mr-3"><img v-bind:src="user.profilePicture" class="rounded mb-2 img-thumbnail profile-picture"></div>
			                    <div class="media-body mb-5 text-white">
			                        <h4 class="mt-0 mb-0">{{user.name}} {{user.surname}}</h4>
			                        <p class="small mb-4"> <i class="fas fa-map-marker-alt mr-2"></i>{{user.dateOfBirth}}</p>
			                    </div>
			                </div>
			            </div>
						<div class="bg-light p-4 d-flex justify-content-end text-center">
			                <ul class="list-inline mb-0">
			                    <li class="list-inline-item">
			                        <button v-if="loggedUser && !isFriend" v-on:click="sendFriendRequest()" class="btn btn-primary" id="sendRequestButton">Send Request</button>
									<button v-else-if="loggedUser" v-on:click="unfriend" class="btn btn-danger" id="sendRequestButton">Unfriend</button>
			                    </li>
			                    <li class="list-inline-item">
			                        <button v-on:click="sendMessage()" v-if="loggedUser" class="btn btn-primary">Send message</button>
			                    </li>
			                </ul>
			            </div>
			            <div v-if="loggedUser && mutualFriends"  class="px-4 py-3">
			                <h5 class="mb-0">Mutual friends</h5>
			                <div class="container testimonial-group">
							  <div class="row text-center">
								 	<div v-for="friend in mutualFriends" v-on:click="selectedUser(friend)" class="col-4">
										<img v-bind:src="friend.profilePicture" width="100" class="rounded mb-2 img-thumbnail">
										<p>{{friend.name}} {{friend.surname}}</p>
									</div>
							  </div>
							</div>
			            </div>
			            <div class="py-4 px-2">
							<hr>
			                <div class="d-flex align-items-center justify-content-between mb-3">
			                    <h5 class="mb-0">Recent photos</h5>
			                </div>
			                <div v-if="!user.privateAccount || isFriend || isAdmin()" class="container testimonial-group">
							  <div class="row text-center">
							 		<div v-for="picture in user.pictures" v-if="!picture.deleted"  v-on:click="selectPicture(picture)"  class="col-4">
										<img v-bind:src="picture.picture" width="300" class="rounded mb-2 img-thumbnail">
									</div>
							  </div>
							</div>
							<div v-else class="container">No posts to show</div>
			            </div>
						<hr>
						<h5 class="mb-0">Recent posts</h5>
						<div v-if="!user.privateAccount || isFriend || isAdmin()" class="d-flex align-items-center justify-content-center">
							<table border='1'>
								<tr>
									<td>
										<div v-for="post in user.posts" v-if="!post.deleted" v-on:click="selectPosts(post, user)" class="card" style="width: 18rem;">
										  <img v-if="post.picture" v-bind:src="post.picture" class="card-img-top" alt="...">
										  <div class="card-body">
										    <h5 class="card-title">{{user.name}} {{user.surname}}</h5>
										    <p class="card-text">{{post.text}}</p>
											<a v-if="isAdmin()" v-on:click.stop="removePost(post.id)" href="#" class="btn btn-primary">Remove</a>
										  </div>
										</div>
									</td>
								</tr>
							</table>
						</div>
						<div v-else class="container">No posts to show</div>
			        </div>
			    </div>
				<link rel="stylesheet" href="css/profile.css" type="text/css">
			</div>
	`
	,
	methods:{
		selectedUser:function(user){
			console.log(user.username);
			
			this.$root.$emit('profile',{
					user:user
			});
		},
		selectPosts: function(post, user){
			if(this.loggedUser != null)
				this.$router.push({name:'post', params:{post:post, user:user}});
		},
		
		selectPicture:function(picture){
			if(this.loggedUser != null)
				this.$router.push({name:'pictureView', params:{picture:picture, user:this.user}});
		},
		
		sendMessage:function(){
			this.$router.push({name:'chat', params:{user:this.user}});
		},
		sendFriendRequest:function(){
			axios.post('rest/user/sendFriendRequest', {sender: this.loggedUser.username, receiver: this.user.username})
						.then((res) => {
							console.log("Uspesno ste poslali zahtev za prijateljstvo.");
							$("#sendRequestButton").text("Request sent").attr("disabled", true);
		                 });
		},
		
		isAdmin:function(){
			if(this.loggedUser == null) return false;
			return this.loggedUser.role == 'ADMIN';
		},
		
		removePost:function(id){
			
			if(this.loggedUser.role == 'ADMIN'){
				
				let reason = prompt("Reason for removing this post:");
				console.log(reason);
				var message = "Your post #"+id+" has been removed by administrator due to: \n'"+reason+"'";
				
				var host = "ws://localhost:8080/SocialMedia/chat";
				console.log(host);
				this.connection = new WebSocket(host);
				// Po uspešnom otvaranju konekcije, ovo se poziva
				var self = this;
				this.connection.onopen = function () { 	
					console.log("Otvorena konekcija.");
				};
				// Greška pri komunikaciji 
				this.connection.onerror = function (error) {
					console.log('WebSocket Error ' + error); 
				};
				// Pri gašenju se ovo poziva
				this.connection.onclose = function(){
					self.connection = null;
				};
			}
				console.log(id);
				axios.delete('rest/feed/deletePost/' + id)
				.then((res) => {
					this.user = res.data;
					if(this.loggedUser.role == 'ADMIN'){
						axios.post('rest/dm/sendMessage', {
						fromUser: this.loggedUser.username,
						toUser: this.user.username,
						content: message})
						.then((res) => {
							this.connection.send(message);
		                 });
					}
					});
		},
		
		unfriend:function(){
			var data={
				"username1": this.user.username,
				"username2": this.loggedUser.username
			}
			axios.post('rest/user/unfriend', data)
			.then((res) => {
				window.localStorage.setItem('loggedUser', JSON.stringify(res.data));
				this.loggedUser = res.data;
				this.isFriend = false;
			});
		}
		
		
	},
	mounted(){
		this.user = this.$route.params.user;
		this.loggedUser = JSON.parse(window.localStorage.getItem("loggedUser"));
		document.removeEventListener("backbutton", () => { $router.back() });
		
		if(this.user.username == this.loggedUser.username){
			this.$router.push({name:'myProfile'});
		}
		
		this.$root.$on('profile', (value) =>{
			this.user = value.user;
			//provera da li su prijatelji
			
			console.log(this.user.friends);
			if(this.loggedUser == null)
				this.isFriend = false;
			else if(this.user.friends.includes(this.loggedUser.username))
				this.isFriend = true;
			console.log("Da li su prijatelji: "+this.isFriend);
			
			//provera da li korisnik ima zajednickih prijatelja
			if(this.loggedUser != null){
				this.mutualFriendsUsername = [];
				for(loggedUserFriend of this.loggedUser.friends){
					console.log(loggedUserFriend);
					for(userFriend of this.user.friends)
						if(loggedUserFriend == userFriend)
							this.mutualFriendsUsername.push(userFriend);
				}
				this.mutualFriendsUsername = (this.mutualFriendsUsername.length == 0 ? null:this.mutualFriendsUsername);
				
				for(req of this.user.friendRequests){
					if(req.sender == this.loggedUser.username && req.state == 'REQUESTED')
						$("#sendRequestButton").text("Request sent").attr("disabled", true);
				}	
			}
			
			
			var data={
				"list":this.mutualFriendsUsername
			}
			
			axios.post('rest/user/getUsersByUsernames', data)
					.then((res) => {
						console.log(res.data);
						this.mutualFriends = res.data;
	                 });
				
		});
		
		this.$root.$emit('profile',{
					user:this.user
			});
		
	}
})