Vue.component("navbar",{
	data(){
        return{
			showSearchNavbar:false
        }
    },
	template:
	`	
		<div>
			<nav class="navbar navbar-expand-lg navbar-light bg-light">
			  <div class="container-fluid">
			    <a class="navbar-brand" href="#/myProfile">My profile</a>
			    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
			      <span class="navbar-toggler-icon"></span>
			    </button>
			    <div class="collapse navbar-collapse" id="navbarNavDropdown">
			      <ul class="navbar-nav">
			        <li class="nav-item">
			          <a class="nav-link active" aria-current="page" href="#/feed">Home page</a>
			        </li>
			        <li class="nav-item">
			          <a class="nav-link" href="#/chat">Messages</a>
			        </li>
					<li class="nav-item">
			          <a class="nav-link" href="#/requests">Friend requests</a>
			        </li>
					<li class="nav-item">
			          <a class="nav-link" href="#/friends">Friends</a>
			        </li>
			        <li class="nav-item">
			          <a class="nav-link" href="#/settings">Settings</a>
			        </li>
			        <li class="nav-item dropdown">
			        </li>
			        <li>
			           <button class="btn btn-outline-success" v-on:click="showSearch()" type="button">Search</button>
			        </li>
					<li class="nav-item" align="right">
						<button class="btn btn-outline-success" v-on:click="logout()">Log out</button>
					</li>
			      </ul>
			    </div>
			  </div>
			</nav>
			<search v-if="showSearchNavbar"></search>
		</div>
		
	`
	,
	methods:{
		logout:function(){
			console.log("logging out...")
			window.localStorage.clear();
			this.$router.push('/');
		},
		showSearch:function(){
			this.showSearchNavbar = !this.showSearchNavbar;
		}
	}
	,
	mounted(){
		
		
	}
})