Vue.component("search",{
	data(){
        return{
            form: {
                firstName: '',
                lastName: '',
                date1: null,
                date2: null
            },
			loggedUser: null,
        }
    },
	template:
	`	
		<div class="container-fluid">
		 	<nav class="navbar navbar-expand-lg navbar-light bg-light">
				  <div class="container-fluid">
				    <a class="navbar-brand" href="#">SearchBar</a>
				    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
				      <span class="navbar-toggler-icon"></span>
				    </button>
				    <form>
						<form v-on:submit.prevent="search()" class="form-inline">
						  <div class="input-group">
							<input class="form-control mr-sm-2" type="search" placeholder="Name" aria-label="Search" v-model="form.firstName">
							<input class="form-control mr-sm-2" type="search" placeholder="Last name" aria-label="Search" v-model="form.lastName">
							<input class="form-control mr-sm-2" type="date" aria-label="Search" v-model="form.date1" min="1900-01-01" max="2022-01-01">
							<input class="form-control mr-sm-2" type="date" aria-label="Search" v-model="form.date2" min="1900-01-01" max="2022-01-01">
							<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
							<a v-if="!loggedUser" class="nav-link" href="#/login">Log in</a>
						  </div>
						</form>
				    </form>
				  </div>
			</nav>
		</div>
	`
	,
	methods:{
		search:function(){
			this.$router.push({name:'resultPage', params: {
				firstName: this.form.firstName,
				lastName: this.form.lastName,
				date1: this.form.date1,
				date2: this.form.date2
			}});
			
			this.$root.$emit('form',{
					firstName: this.form.firstName,
					lastName: this.form.lastName,
					date1: this.form.date1,
					date2: this.form.date2
			});
		},
	}
	,
	mounted(){
		this.loggedUser = JSON.parse(window.localStorage.getItem("loggedUser"));
		document.removeEventListener("backbutton", () => { $router.go(-1) });
		this.form.date1 = "1900-01-01";
		this.form.date2 = "2022-01-01";
	}
})