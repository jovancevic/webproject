Vue.component("requests",{
	data(){
        return{
			loggedUser:null,
			users:null
        }
    },
	template:
	`	
		<div>
			<div v-if="loggedUser && users && this.loggedUser.friendRequests">
				<navbar />
				<div v-if="users.length > 0" class="container">
					<h1>Friend requests</h1><br>
					<div class="row">
						<div class="col-md-8">
							<div class="people-nearby">
							  
							  <div v-for="request in loggedUser.friendRequests" v-if="request.state == 'REQUESTED'" class="nearby-user">
								<div class="row">
								  <div class="col-md-2 col-sm-2">
									<img v-bind:src="getProfilePicture(request.sender)" alt="user" class="profile-photo-lg">
								  </div>
								  <div class="col-md-7 col-sm-7">
									<h5><a v-on:click="selectUser(request.sender)" href="#" class="profile-link">{{getName(request.sender)}}</a></h5>
								  </div>
								  <div class="col-md-3 col-sm-3">
									<button v-on:click="respondToRequest(request, true)" class="btn btn-primary pull-right">Accept</button>
									<button v-on:click="respondToRequest(request, false)" class="btn btn-danger">Decline</button>
								  </div>
								</div>
							  </div>
							</div>
						</div>
					</div>
				</div>
				<div v-else class="container"><h1>No Friend requests</h1></div>
				<link rel="stylesheet" href="css/request.css" type="text/css"/>
			</div>
		</div>
	`
	,
	methods:{
		
		selectUser:function(sender){
			for(u of this.users){
				if(u.username == sender)
					this.$router.push({name:'profile', params:{user:u}});
			}
		},
		
		getName:function(username){
			for(user of this.users){
				if(user.username == username)
					return user.name + " " + user.surname;
			}
		},
		
		getProfilePicture:function(username){
			for(user of this.users){
				if(user.username == username)
					return user.profilePicture;
			}
		},
		
		respondToRequest:function(request, accepted){
			console.log(request)
			var data={
			"requestId": request.id,
			"accepted": accepted
		}
			axios.post('rest/user/respondToRequest', data)
				.then((res) => {
					this.loggedUser.friendRequests = this.loggedUser.friendRequests.filter(
					function(ele){ 
						return ele.id != request.id;
					});
					window.localStorage.setItem('loggedUser', JSON.stringify(res.data));
                 });
		},
		
	},
	
	mounted(){
		this.loggedUser = JSON.parse(window.localStorage.getItem("loggedUser"));
		
		if(!this.loggedUser){
			this.$router.push({name:'login'});
			return;
		}
		document.removeEventListener("backbutton", () => { $router.go(-1) });
		
		var usernames = [];
		console.log(this.loggedUser);
		for(request of this.loggedUser.friendRequests){
			if(request.state == 'REQUESTED'){
				usernames.push(request.sender);
			}
		}
		
		var data={
			"list": usernames
		}
		/*
		axios.get('rest/user/getRequests', {params:{username: this.loggedUser.username}
		}).then((res) => {
					this.loggedUser.friendRequests = res.data;
					window.localStorage.setItem("loggedUser", JSON.stringify(this.loggedUser));
                
		});
		*/
		
		axios.post('rest/user/getUsersByUsernames', data)
				.then((res) => {
					this.users = res.data;
                 });
		
	}
})