Vue.component("myProfile",{
	data(){
        return{
            file: null,
            loggedUser: null,
            pictureDescription: "",
            postText: "",
        }
    },
	template:
	`	
		<div>
			<navbar />
			<div v-if="loggedUser" class="row py-5 px-4">
			    <div class="col-md-5 mx-auto">
			        <!-- Profile widget -->
			        <div class="bg-white shadow rounded overflow-hidden">
			            <div class="px-4 pt-0 pb-5 cover">
			                <div class="media align-items-end profile-head">
			                    <div class="profile mr-3"><img v-bind:src="loggedUser.profilePicture" alt="..." width="130" class="rounded mb-2 img-thumbnail"></div>
			                    <div class="media-body mb-5 text-white">
			                        <h4 class="mt-0 mb-0">{{loggedUser.name}} {{loggedUser.surname}}</h4>
			                        <p class="small mb-4"> <i class="fas fa-map-marker-alt mr-2"></i>{{loggedUser.dateOfBirth}}</p>
			                    </div>
			                </div>
			            </div>
						<div class="bg-light p-4 d-flex justify-content-center text-center">
			                <ul class="list-inline mb-0">
			                    <li class="list-inline-item">
			                        <input id="fileInputPic" type="file"/>
			                        <div class="form-group">
									    <label for="textarea2">Write description:</label>
									    <textarea v-model="pictureDescription" class="form-control" id="textarea2" rows="3"></textarea>
									  </div>
			                        <button v-on:click="uploadPicture" class="btn btn-primary">Upload picture</button>
			                    </li>
								<hr>
			                    <li class="list-inline-item">
			                    	<input id="fileInputPost" type="file"/>
			                    	<div class="form-group">
									    <label for="textarea1">Write post:</label>
									    <textarea v-model="postText" class="form-control" id="textarea1" rows="3"></textarea>
									  </div>
			                    	<button v-on:click="uploadPost" class="btn btn-primary">Add post</button>
			                    </li>
			                </ul>
			            </div>
			            
			            <div class="py-4 px-2">
			                <div class="d-flex align-items-center justify-content-between mb-3">
			                    <h5 class="mb-0">Recent photos</h5>
			                </div>
			                <div class="container testimonial-group">
							  <div class="row text-center">
							 		<div v-for="picture in loggedUser.pictures" v-if="!picture.deleted" class="col-4">
										<img v-bind:src="picture.picture" v-on:click="selectPicture(picture)" width="300" class="rounded mb-2 img-thumbnail"><br>
										<button v-on:click="setProfilePicture(picture.id)" class="btn btn-primary">Set as profile</button>
									</div>
							  </div>
							</div>
			            </div>
						<hr>
						<h5 class="mb-0" style="padding-left: 0.5rem">Recent posts</h5>
						<div class="d-flex align-items-center justify-content-center">
							<table border='1'>
								<tr>
									<td>
										<div v-for="post in loggedUser.posts" v-if="!post.deleted" v-on:click="selectPosts(post)" class="card" style="width: 18rem;">
										  <img v-if="post.picture" v-bind:src="post.picture" class="card-img-top" alt="...">
										  <div class="card-body">
										    <h5 class="card-title">{{loggedUser.name}} {{loggedUser.surname}}</h5>
										    <p class="card-text">{{post.text}}</p>
											<a v-on:click.stop="removePost(post.id)" href="#" class="btn btn-primary">Remove</a>
										  </div>
										</div>
									</td>
								</tr>
							</table>
						</div>
			        </div>
			    </div>
			</div>
			<link rel="stylesheet" href="css/profile.css" type="text/css">
		</div>
		
	`
	,
	methods:{
		setProfilePicture(id){
			axios({
				  method: 'POST',
				  url: 'rest/user/changeProfilePicture',
				  data: {
						 pictureId : id,
						 username: this.loggedUser.username	},
				}).then((res) => {
					this.loggedUser = res.data;
					window.localStorage.setItem('loggedUser', JSON.stringify(res.data));
                 });
		},
		
		removePost:function(id){
			console.log(id);
			axios.delete('rest/feed/deletePost/' + id)
			.then((res) => {
				this.loggedUser = res.data;
				window.localStorage.setItem('loggedUser', JSON.stringify(this.loggedUser));
			});
		},
		uploadPost:function(){
			if(!this.postText){
				alert("Please write something to post.");
				return;
			}
			var file = null;
			var reader = new FileReader();
			var sendData;
			if(document.getElementById("fileInputPost").files.length != 0){
				file = document.getElementById("fileInputPost").files[0];
				console.log(file);
				
			}	
			var self = this;
			
			if(!file){
				sendData = self.loggedUser.username + "&" + self.postText;
				axios({
				  method: 'POST',
				  url: 'rest/feed/uploadPost',
				  data: {data : sendData},
				}).then((res) => {
					self.loggedUser.posts.unshift(res.data);
					window.localStorage.setItem('loggedUser', JSON.stringify(self.loggedUser));
					self.postText = "";
                 });
				return;
			}
				
			reader.onloadend = function(){
				var b64 = reader.result.replace(/^data:.+;base64,/, '');
				sendData = self.loggedUser.username + "&" + self.postText + "&" + b64;
						
				axios({
				  method: 'POST',
				  url: 'rest/feed/uploadPost',
				  data: {data : sendData},
				}).then((res) => {
					self.loggedUser.posts.unshift(res.data);
					window.localStorage.setItem('loggedUser', JSON.stringify(self.loggedUser));
					self.postText = "";
                 });
			}
			reader.readAsDataURL(file);
		},
		uploadPicture:function(){
			if(document.getElementById("fileInputPic").files.length == 0){
				alert("Please choose picture to post.");
				return;
			}
			var file = document.getElementById("fileInputPic").files[0];
			console.log(file);
			var reader = new FileReader();
			
			var self = this;
			
			reader.onloadend = function(){
				var b64 = reader.result.replace(/^data:.+;base64,/, '');
				var sendData = self.loggedUser.username + "&" + self.pictureDescription + "&" + b64;
			
				axios({
				  method: 'POST',
				  url: 'rest/feed/uploadPicture',
				  data: {data : sendData},
				}).then((res) => {
					console.log("Uspesno postavljanje slike.");
					console.log(res.data);
					self.loggedUser.pictures.unshift(res.data);
					window.localStorage.setItem('loggedUser', JSON.stringify(self.loggedUser));
					self.pictureDescription = "";
                 });
			}
			reader.readAsDataURL(file);
		},
		selectPosts: function(post){
			this.$router.push({name:'post', params:{post:post, user:this.loggedUser}});
		},
		selectPicture:function(picture){
			this.$router.push({name:'pictureView', params:{picture:picture, user:this.loggedUser}});
		}
	}
	,
	mounted(){
		this.loggedUser = JSON.parse(window.localStorage.getItem('loggedUser'));
		
		if(!this.loggedUser){
			this.$router.push({name:'login'});
			return;
		}
		document.removeEventListener("backbutton", () => { $router.go(-1) });
	}
})