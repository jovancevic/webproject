package utility;

import java.util.List;

import beans.FriendRequest;
import beans.Gender;
import beans.Picture;
import beans.Post;
import beans.Role;
import beans.User;

public class UserDTO {
	private String username;
	private String password;
	private String email;
	private String name;
	private String surname;
	private String dateOfBirth;
	private Gender gender;
	private Role role;
	private String profilePicture;
	private List<String> friends;
	private boolean privateAccount;
	private boolean blocked;
	
	public UserDTO() {
		
	}
	
	public UserDTO(User u) {
		super();
		this.username = u.getUsername();
		this.password = u.getPassword();
		this.email = u.getEmail();
		this.name = u.getName();
		this.surname = u.getSurname();
		this.dateOfBirth = u.getDateOfBirth();
		this.gender = u.getGender();
		this.role = u.getRole();
		this.profilePicture = u.getProfilePicture();
		this.friends = u.getFriends();
		this.privateAccount = u.isPrivateAccount();
		this.blocked = u.isBlocked();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

	public List<String> getFriends() {
		return friends;
	}

	public void setFriends(List<String> friends) {
		this.friends = friends;
	}

	public boolean isPrivateAccount() {
		return privateAccount;
	}

	public void setPrivateAccount(boolean privateAccount) {
		this.privateAccount = privateAccount;
	}

	public boolean isBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}
	
	
	
	
}
