package beans;

public enum FriendRequestState {
	ACCEPTED,
	DECLINED,
	REQUESTED
}
