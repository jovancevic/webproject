package beans;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {
	private String username;
	private String password;
	private String email;
	private String name;
	private String surname;
	private String dateOfBirth;
	private Gender gender;
	private Role role;
	private String profilePicture;
	private List<Post> posts;
	private List<Picture> pictures;
	private List<FriendRequest> friendRequests;
	private List<String> friends;
	private boolean privateAccount;
	private boolean blocked;
	
	public User() {}

	public User(String username, String password, String email, String name, String surname, String dateOfBirth,
			Gender gender, Role role, String profilePicture, List<Post> posts, List<Picture> pictures,
			List<FriendRequest> friendRequests, List<String> friends, boolean privateAccount, boolean blocked) {
		super();
		this.username = username;
		this.password = password;
		this.email = email;
		this.name = name;
		this.surname = surname;
		this.dateOfBirth = dateOfBirth;
		this.gender = gender;
		this.role = role;
		this.profilePicture = profilePicture;
		this.posts = posts;
		this.pictures = pictures;
		this.friendRequests = friendRequests;
		this.friends = friends;
		this.privateAccount = privateAccount;
		this.blocked = blocked;
	}
	
	public boolean isBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	public User(String username, String password, String email, String name, String surname, String dateOfBirth,
			Gender gender, Role role, String profilePicture, boolean privateAccount) {
		super();
		this.username = username;
		this.password = password;
		this.email = email;
		this.name = name;
		this.surname = surname;
		this.dateOfBirth = dateOfBirth;
		this.gender = gender;
		this.role = role;
		this.profilePicture = profilePicture;
		this.posts = new ArrayList<>();
		this.pictures = new ArrayList<>();;
		this.friendRequests = new ArrayList<>();;
		this.friends = new ArrayList<>();;
		this.privateAccount = privateAccount;
	}
	
	public void changeInformation(String password, String email, String name, String surname, String dateOfBirth,
			Gender gender, boolean privateAccount) {
		this.password = password;
		this.email = email;
		this.name = name;
		this.surname = surname;
		this.dateOfBirth = dateOfBirth;
		this.gender = gender;
		this.privateAccount = privateAccount;
	}
	
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

	public List<Post> getPosts() {
		return posts;
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

	public List<Picture> getPictures() {
		return pictures;
	}

	public void setPictures(List<Picture> pictures) {
		this.pictures = pictures;
	}

	public List<FriendRequest> getFriendRequests() {
		return friendRequests;
	}

	public void setFriendRequests(List<FriendRequest> friendRequests) {
		this.friendRequests = friendRequests;
	}

	public List<String> getFriends() {
		return friends;
	}

	public void setFriends(List<String> friends) {
		this.friends = friends;
	}

	public boolean isPrivateAccount() {
		return privateAccount;
	}

	public void setPrivateAccount(boolean privateAccount) {
		this.privateAccount = privateAccount;
	}
	
	public void addFriendRequest(FriendRequest fr) {
		this.friendRequests.add(fr);
	}
	
	public void addPost(Post post) {
		this.posts.add(post);
	}
	
	public void addPicture(Picture pic) {
		this.pictures.add(pic);
	}
	public void removeFriend(String u1) {
		this.friends.remove(u1);
	}
	
	public void addFriend(String username) {
		this.friends.add(username);
	}

	@Override
	public String toString() {
		return "User [username=" + username + ", email=" + email + ", name=" + name + ", surname=" + surname
				+ ", dateOfBirth=" + dateOfBirth + ", gender=" + gender + ", role=" + role + ", privateAccount="
				+ privateAccount + "]";
	}

	public void removePost(int id) {
		for(Post p : this.posts)
			if(p.getId() == id)
				p.setDeleted(true);
	}
	
	public void removePicture(int id) {
		for(Picture p : this.pictures)
			if(p.getId() == id)
				p.setDeleted(true);
	}

	public void changePost(int id, Post p1) {
		for(Post p : this.posts)
			if(p.getId() == id) {
				p = p1;
			}
	}
	
	public void changePicture(int id, Picture p1) {
		for(Picture p : this.pictures)
			if(p.getId() == id) {
				p = p1;
			}
	}

	public void removePictureComment(int idPic, int idCom) {
		for(Picture p : this.pictures)
			if(p.getId() == idPic) {
				for(Comment c : p.getComments()) {
					if(c.getId() == idCom) 
						c.setDeleted(true);
				}
			}
	}
	
	public void removePostComment(int idPost, int idCom) {
		for(Post p : this.posts)
			if(p.getId() == idPost) {
				for(Comment c : p.getComments()) {
					if(c.getId() == idCom) 
						c.setDeleted(true);
				}
			}
	}
	
}
