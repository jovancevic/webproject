package beans;

import java.time.LocalDate;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class FriendRequest {
	
	private String sender;
	private String recipient;
	private FriendRequestState state;
	private String date;
	static int idCounter = 1;
	private int id;
	
	public FriendRequest() {
		this.id = idCounter++;
	}

	public FriendRequest(String sender, String recipient, FriendRequestState state, String date) {
		super();
		this.sender = sender;
		this.recipient = recipient;
		this.state = state;
		this.date = date;
		this.id = idCounter++;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public FriendRequestState getState() {
		return state;
	}

	public void setState(FriendRequestState state) {
		this.state = state;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	public int getId() {
		return this.id;
	}

	@Override
	public String toString() {
		return "FriendRequest [sender=" + sender + ", recipient=" + recipient + ", state=" + state + ", date=" + date
				+ ", id=" + id + "]";
	}
	
}
