package beans;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.print.attribute.standard.MediaSize.ISO;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import utility.LocalDateTimeDeserializer;
import utility.LocalDateTimeSerializer;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class DirectMessage implements Comparable{
	private String sender;
	private String recipient;
	private String content;
	//@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime dateTime;
	
	public DirectMessage() {}

	public DirectMessage(String sender, String recipient, String content, LocalDateTime dateTime) {
		super();
		this.sender = sender;
		this.recipient = recipient;
		this.content = content;
		this.dateTime = dateTime;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public LocalDateTime getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime dt = LocalDateTime.parse(dateTime, formatter);
		this.dateTime = dt;
	}

	@Override
	public String toString() {
		return "DirectMessage [sender=" + sender + ", recipient=" + recipient + ", content=" + content + ", date="
				+ dateTime + "]";
	}

	@Override
	public int compareTo(Object o) {
		DirectMessage other = (DirectMessage) o;
		if(this.dateTime.isAfter(other.getDateTime()))
			return 1;
		else if(this.dateTime.isBefore(other.getDateTime()))
			return -1;
		return 0;
	}
}
