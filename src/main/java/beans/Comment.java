package beans;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import utility.LocalDateDeserializer;
import utility.LocalDateSerializer;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class Comment {
	private String publisher;
	@JsonDeserialize(using = LocalDateDeserializer.class)  
	@JsonSerialize(using = LocalDateSerializer.class)  
	private LocalDate date;
	@JsonDeserialize(using = LocalDateDeserializer.class)  
	@JsonSerialize(using = LocalDateSerializer.class)  
	private LocalDate editDate;
	private String text;
	private boolean deleted;
	static int idCounter = 1;
	private int id;
	
	public Comment() {
		this.id = idCounter++;
	}
	
	public Comment(Comment c1) {
		super();
		this.publisher = c1.getPublisher();
		this.date = c1.getDate();
		this.editDate = c1.getEditDate();
		this.text = c1.getText();
		this.deleted = c1.isDeleted();
		this.id = c1.getId();
	}

	
	public Comment(String publisher, LocalDate date, String text) {
		this.publisher = publisher;
		this.date = date;
		this.editDate = null;
		this.text = text;
		this.deleted = false;
		this.id = idCounter++;
	}

	public Comment(String publisher, LocalDate date, LocalDate editDate, String text, boolean deleted) {
		super();
		this.publisher = publisher;
		this.date = date;
		this.editDate = editDate;
		this.text = text;
		this.deleted = deleted;
		this.id = idCounter++;
	}

	public String getText() {
		return text;
	}
	
	public int getId() {
		return this.id;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	
	
	public boolean isDeleted() {
		return deleted;
	}
	
	
	
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}


	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public LocalDate getEditDate() {
		return editDate;
	}

	public void setEditDate(LocalDate editDate) {
		this.editDate = editDate;
	}

	@Override
	public String toString() {
		return "Comment [publisher=" + publisher + ", date=" + date + ", editDate=" + editDate + "]";
	}
	
	
}
