package beans;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class Picture {
	private String username;
	private String picture;
	private String text;
	private List<Comment> comments;
	private boolean deleted;
	static int idCounter = 1;
	private int id;
	
	public Picture() {
		this.id = idCounter++;
	}
	
	public Picture(Picture p1) {
		super();
		this.username = p1.getUsername();
		this.picture = p1.getPicture();
		this.text = p1.getText();
		this.comments = p1.getComments();
		this.deleted = p1.isDeleted();
		this.id = p1.getId();
	}
	
	public Picture(String username, String picture, String text) {
		this.username = username;
		this.picture = picture;
		this.text = text;
		this.id = idCounter++;
		this.comments = new ArrayList<Comment>();
		this.deleted = false;
	}

	public Picture(String username, String picture, String text, List<Comment> comments, boolean deleted) {
		super();
		this.username = username;
		this.picture = picture;
		this.text = text;
		this.comments = comments;
		this.id = idCounter++;
		this.deleted = deleted;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	
	public int getId() {
		return this.id;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	@Override
	public String toString() {
		return "Picture [username=" + username + ", picture=" + picture + ", text=" + text + ", comments=" + comments
				+ ", deleted=" + deleted + ", id=" + id + "]";
	}

	public void addComment(Comment comment) {
		this.comments.add(comment);
		
	}
	
}
