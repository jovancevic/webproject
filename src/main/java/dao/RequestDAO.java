package dao;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import beans.FriendRequest;
import beans.Post;
import beans.User;

public class RequestDAO {
	private List<FriendRequest> requests = new ArrayList<>();
	String path;
	
	public RequestDAO() {}
	
	public RequestDAO(String contextPath){
		this.path = contextPath;
		try {
			loadRequests(contextPath);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void loadRequests(String contextPath) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		
		this.requests = mapper.readValue(new File(contextPath), new TypeReference<List<FriendRequest>>(){});
		
	}
	
	public List<FriendRequest> getUsersRequests(String username){
		List<FriendRequest> retList = new ArrayList<FriendRequest>();
		for(FriendRequest p : this.requests) {
			if(p.getRecipient().equals(username))
				retList.add(p);
		}
		return retList;
	}
	
	public FriendRequest getRequestById(int id) {
		for(FriendRequest r : requests) {
			if(r.getId() == id)
				return r;
		}
		return null;
	}
	
	public void addFriendRequest(FriendRequest f) {
		requests.add(f);
	}
	
	public void saveChanges() throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
	
		mapper.writeValue(new File(path), requests);
	
	}
	
}
