package dao;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import beans.Comment;
import beans.Picture;
import beans.Post;

public class PictureDAO {

	private Map<Integer, Picture> pictures = new HashMap<>();
	String path;
	
	public PictureDAO() {
		
	}
	
	public PictureDAO(String contextPath) {
		this.path = contextPath;
		try {
			loadPictures(contextPath);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void loadPictures(String contextPath) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		
		List<Picture> pictureData = mapper.readValue(new File(contextPath), new TypeReference<List<Picture>>(){});
		for(Picture p : pictureData) {
			pictures.put(p.getId(), p);
		}	
	}
	
	public List<Picture> getUsersPictures(String username){
		List<Picture> retList = new ArrayList<Picture>();
		for(Picture p : pictures.values()) {
			if(p.getUsername().equals(username))
				retList.add(p);
		}
		return retList;
	}
	
	public Picture addPicture(String username, String picturePath, String text) {
		System.out.println("add picture:"+username);
		Picture p = new Picture(username, picturePath, text);
		pictures.put(p.getId(), p);
		return p;
	}
	
	public Picture getPictureById(int id) {
		return pictures.get(id);
	}
	
	public void removePicture(int id) {
		pictures.get(id).setDeleted(true);
	}
	
	public Picture removeCommentById(int pictureId, int commentId) {
		Picture p = pictures.get(pictureId);
		for(Comment c : p.getComments()) {
			if(c.getId() == commentId) {
				c.setDeleted(true);
			}
		}
		return p;
	}
	
	public void saveChanges() throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		
		List<Picture> pictureList  = new ArrayList<>();
		for(Map.Entry<Integer, Picture> entry : pictures.entrySet()) {
			pictureList.add(entry.getValue());
		}
		System.out.println(path);
		mapper.writeValue(new File(path), pictureList);
	
	}
	
	
}
