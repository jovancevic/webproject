package dao;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import beans.Comment;
import beans.Post;
import beans.User;

public class PostDAO {
	
	private Map<Integer, Post> posts = new HashMap<>();
	String path;
	
	public PostDAO() {}
	
	public PostDAO(String contextPath){
		this.path = contextPath;
		try {
			loadPosts(contextPath);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void loadPosts(String contextPath) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		
		List<Post> postData = mapper.readValue(new File(contextPath), new TypeReference<List<Post>>(){});
		for(Post p : postData) {
			posts.put(p.getId(), p);
		}	
	}
	
	public List<Post> getUsersPosts(String username){
		List<Post> retList = new ArrayList<Post>();
		for(Post p : posts.values()) {
			if(p.getUsername().equals(username))
				retList.add(p);
		}
		return retList;
	}
	
	public Post addPost(String username, String text, String picturePath) {
		Post p = new Post(username, picturePath, text);
		posts.put(p.getId(), p);
		return p;
	}
	
	public Post getPostById(int id) {
		return posts.get(id);
	}
	
	public void removePost(int id) {
		posts.get(id).setDeleted(true);
	}

	public Post removeCommentById(int postId, int commentId) {
		Post p = posts.get(postId);
		for(Comment c : p.getComments()) {
			if(c.getId() == commentId) {
				c.setDeleted(true);
			}
		}
		return p;
	}
	
	public void saveChanges() throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		
		List<Post> postList  = new ArrayList<>();
		for(Map.Entry<Integer, Post> entry : posts.entrySet()) {
			postList.add(entry.getValue());
		}
		System.out.println(path);
		mapper.writeValue(new File(path), postList);
	
	}
	
	
}
