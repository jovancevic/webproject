package dao;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import beans.DirectMessage;
import beans.User;

public class MessagesDAO {
	private List<DirectMessage> messages = new ArrayList<>();
	String path;
	
	public MessagesDAO() {}
	
	public MessagesDAO(String contextPath){
		this.path = contextPath;
		try {
			loadMessages(contextPath);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void loadMessages(String contextPath) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		
		this.messages = mapper.readValue(new File(contextPath), new TypeReference<List<DirectMessage>>(){});
	}
	
	public List<DirectMessage> getMessagesBetweenUsers(String username1, String username2){
		List<DirectMessage> retList = new ArrayList<>();
		
		for(DirectMessage m : this.messages) {
			if((m.getRecipient().equals(username1) &&  m.getSender().equals(username2)) 
					|| (m.getRecipient().equals(username2) &&  m.getSender().equals(username1))) {
				
				retList.add(m);
			}
		}
		Collections.sort(retList);
		
		return retList;
	}
	
	public void addMessage(DirectMessage m) {
		this.messages.add(m);
		
	}

	public List<String> getMessagedUsers(String loggedUser) {
		List<String> messagedUsers = new ArrayList<String>();
		for (DirectMessage message : this.messages) {
			if(message.getRecipient().equals(loggedUser) || message.getSender().equals(loggedUser)) {
				String user = message.getRecipient().equals(loggedUser)?message.getSender():message.getRecipient();
				if(!messagedUsers.contains(user))
					messagedUsers.add(user);
			}
		}
		return messagedUsers;
	}
	
	public void saveChanges() throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
	
		mapper.writeValue(new File(path), messages);
	
	}
	
}
