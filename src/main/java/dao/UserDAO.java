package dao;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import beans.FriendRequest;
import beans.FriendRequestState;
import beans.Picture;
import beans.Post;
import beans.User;
import utility.UserDTO;

public class UserDAO {
	private Map<String, User> users = new HashMap<>();
	PostDAO postDAO;
	RequestDAO requestDAO;
	PictureDAO pictureDAO;
	String path;
	
	public UserDAO() {}
	
	public UserDAO(String contextPath, PostDAO postDAO, RequestDAO requestDAO, PictureDAO pictureDAO){
		this.postDAO = postDAO;
		this.requestDAO = requestDAO;
		this.pictureDAO = pictureDAO;
		this.path = contextPath;
		try {
			loadUsers(contextPath);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void loadUsers(String contextPath) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		
		List<User> userData = mapper.readValue(new File(contextPath), new TypeReference<List<User>>(){});
		for(User u : userData) {
			u.setPosts(postDAO.getUsersPosts(u.getUsername()));
			u.setFriendRequests(requestDAO.getUsersRequests(u.getUsername()));
			u.setPictures(pictureDAO.getUsersPictures(u.getUsername()));
			users.put(u.getUsername(), u);
		}	
	}
	
	
	public String readFileAsString(String file)throws Exception
    {
        return new String(Files.readAllBytes(Paths.get(file)));
    }
	
	public User find(String username, String password) {
		if (!users.containsKey(username)) {
			return null;
		}
		User user = users.get(username);
		if (!user.getPassword().equals(password)) {
			return null;
		}
		return user;
	}
	
	public void addUser(User user) {
		this.users.put(user.getUsername(),user);
	}
	
	public User getByUsername(String username) {
		return users.get(username);
	}
	
	public Collection<User> findAll() {
		return users.values();
	}
	
	public Collection<User> search(String firstName, String lastName, String date1, String date2) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date from = null;
		Date to = null;
		Collection<User> searchedUsers = new ArrayList<>();
		try {
			from = formatter.parse(date1);
			to = formatter.parse(date2);
		}catch(Exception ex) {
			return searchedUsers;
		}
		
		for (User user : users.values()) {
			Date birth = formatter.parse(user.getDateOfBirth());
			
			if((user.getName().toLowerCase().contains(firstName.toLowerCase()) || firstName.toLowerCase().contains(user.getName().toLowerCase()))
					&& (user.getSurname().toLowerCase().contains(lastName.toLowerCase()) || lastName.toLowerCase().contains(user.getSurname().toLowerCase()))
					&& birth.before(to) && birth.after(from)) {
				searchedUsers.add(user);
			}
		}
		return searchedUsers;
	}
	
	public Collection<FriendRequest> getFriendRequests(String username){
		Collection<FriendRequest> requests = new ArrayList<>();
		for(FriendRequest req : this.getByUsername(username).getFriendRequests()) {
			if(req.getState() == FriendRequestState.REQUESTED)
				requests.add(req);
		}
		return requests;
	}

	public Collection<User> searchAdmin(String input) {
		Collection<User> searchedUsers = new ArrayList<>();
		
		for (User user : users.values()) {
			if((user.getName().toLowerCase().contains(input.toLowerCase()) || input.toLowerCase().contains(user.getName().toLowerCase()))
					|| (user.getSurname().toLowerCase().contains(input.toLowerCase()) || input.toLowerCase().contains(user.getSurname().toLowerCase()))
					|| (user.getEmail().toLowerCase().contains(input.toLowerCase()) || input.toLowerCase().contains(user.getEmail().toLowerCase()))) {
				searchedUsers.add(user);
			}
		}
		return searchedUsers;
	}

	public User deletePostById(int id) {
		for (User user : users.values()) {
			for(Post p : user.getPosts()){
				if(p.getId() == id) {
					p.setDeleted(true);
					return user;
				}
			}
		}
		return null;
	}

	public void addPost(String username, Post post) {
		User user = this.getByUsername(username);
		user.addPost(post);
		System.out.println(post);
	}

	public User deletePictureById(int id) {
		for (User user : users.values()) {
			for(Picture p : user.getPictures()){
				if(p.getId() == id) {
					p.setDeleted(true);
					return user;
				}
			}
		}
		return null;
	}

	public void unfriend(String receiver, String sender) {
		User u1 = users.get(receiver);
		User u2 = users.get(sender);
		
		u1.removeFriend(u2.getUsername());
		u2.removeFriend(u1.getUsername());
	}

	public void makeFriends(String sender, String recipient) {
		User u1 = users.get(sender);
		User u2 = users.get(recipient);
		u1.addFriend(recipient);
		u2.addFriend(sender);
		
	}

	public void addPicture(String username, Picture pic) {
		User user = this.getByUsername(username);
		user.addPicture(pic);
		
	}

	public User changeProfilePicture(String username, String picturePath) {
		users.get(username).setProfilePicture(picturePath);
		return users.get(username);
	}
	
	public void saveChanges() throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		
		List<UserDTO> userList  = new ArrayList<>();
		for(Map.Entry<String, User> entry : users.entrySet()) {
			userList.add(new UserDTO(entry.getValue()));
		}
		System.out.println(path);
		mapper.writeValue(new File(path), userList);
	
	}

	public void toggleBlock(String username) {
		users.get(username).setBlocked(!users.get(username).isBlocked());
	}

	public int numberOfUserPhotos(String username) {
		return users.get(username).getPictures().size();
	}
	
}