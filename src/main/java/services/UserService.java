package services;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSessionContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import beans.FriendRequest;
import beans.FriendRequestState;
import beans.Gender;
import beans.Picture;
import beans.Role;
import beans.User;
import dao.PictureDAO;
import dao.PostDAO;
import dao.RequestDAO;
import dao.UserDAO;

@Path("/user")
public class UserService {
	
	@Context
	ServletContext ctx;
	
	public UserService() {}
	
	@PostConstruct
	public void init() {
		if (ctx.getAttribute("userDAO") == null) {
			String pathForPosts = ctx.getRealPath("/data/posts.json");
			ctx.setAttribute("postDAO", new PostDAO(pathForPosts));
			
			String pathForRequests = ctx.getRealPath("/data/requests.json");
			ctx.setAttribute("requestDAO", new RequestDAO(pathForRequests));
			
			String pathForPictures = ctx.getRealPath("/data/pictures.json");
			ctx.setAttribute("pictureDAO", new PictureDAO(pathForPictures));
			
	    	String contextPath = ctx.getRealPath("/data/users.json");
			ctx.setAttribute("userDAO", new UserDAO(contextPath, (PostDAO)ctx.getAttribute("postDAO"), (RequestDAO)ctx.getAttribute("requestDAO"), (PictureDAO)ctx.getAttribute("pictureDAO")));
		}
	}
	
	@GET
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<User> getAllUsers() {
		UserDAO userDao = (UserDAO) ctx.getAttribute("userDAO");
		return userDao.findAll();
	}
	
	
	@POST
	@Path("/settings")
	@Consumes(MediaType.APPLICATION_JSON)
	//@Produces(MediaType.APPLICATION_JSON)
	public Response changeInformation(HashMap<String, String> values) throws IOException {
		UserDAO userDao = (UserDAO) ctx.getAttribute("userDAO");
		User existingUser = userDao.getByUsername(values.get("username"));
		
		if(Boolean.parseBoolean(values.get("passwordChange")))
		{
			if(!existingUser.getPassword().equals(values.get("oldPassword"))) {
				return Response.status(400).build(); 
			}
		}
		String password = existingUser.getPassword();
		if(Boolean.parseBoolean(values.get("passwordChange"))){
			password = values.get("password");
		}
		
		existingUser.changeInformation(password, values.get("email"), values.get("name"), values.get("surname"), values.get("dateOfBirth"), Gender.valueOf(values.get("gender")), Boolean.parseBoolean(values.get("private")));
		
		ObjectMapper mapper = new ObjectMapper();
		String jsonString = mapper.writeValueAsString(existingUser);
		
		userDao.saveChanges();
		
		return Response.ok(jsonString, MediaType.APPLICATION_JSON).build();
		
	}
	
	@GET
	@Path("/search")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<User> searchUsers(@QueryParam("firstName") String firstName, @QueryParam("lastName") String lastName,
			@QueryParam("date1") String date1, @QueryParam("date2") String date2) throws ParseException {
		UserDAO userDao = (UserDAO) ctx.getAttribute("userDAO");
		return userDao.search(firstName, lastName, date1, date2);
		
	}
	
	@GET
	@Path("/searchAdmin")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<User> searchUsersAdmin(@QueryParam("input") String input) throws ParseException {

		UserDAO userDao = (UserDAO) ctx.getAttribute("userDAO");
		return userDao.searchAdmin(input);
		
	}
	
	@POST
	@Path("/getUsersByUsernames")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<User> getUsersByUsernames(Map<String, List<String>> values) throws ParseException {
		UserDAO userDao = (UserDAO) ctx.getAttribute("userDAO");
		List<User> users = new ArrayList<User>();
		if(values.get("list") == null)
			return null;
		for (String username : values.get("list")) {
			users.add(userDao.getByUsername(username));
		}
		
		return users;
		
	}
	
	@POST
	@Path("/sendFriendRequest")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response sendFriendRequest(Map<String, String> values) throws ParseException, JsonGenerationException, JsonMappingException, IOException {
		UserDAO userDao = (UserDAO) ctx.getAttribute("userDAO");
		RequestDAO requestDao = (RequestDAO) ctx.getAttribute("requestDAO");
		String receiver = values.get("receiver");
		String sender = values.get("sender");
		
		Date date = new Date();  
	    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");  
	    String strDate= formatter.format(date);  
		FriendRequest f = new FriendRequest(sender, receiver, FriendRequestState.REQUESTED, strDate);
		
		requestDao.addFriendRequest(f);
		userDao.getByUsername(receiver).addFriendRequest(f);
		
		userDao.saveChanges();
		requestDao.saveChanges();
		
		return Response.status(200).build();
		
	}
	
	@GET
	@Path("/toggleBlock")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public User toggleBlock(@QueryParam("username") String username) throws ParseException, JsonGenerationException, JsonMappingException, IOException {

		UserDAO userDao = (UserDAO) ctx.getAttribute("userDAO");
		userDao.toggleBlock(username);
		userDao.saveChanges();
		
		return userDao.getByUsername(username);
		
	}
	
	@POST
	@Path("/unfriend")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public User unfriend(Map<String, String> values) throws ParseException, JsonGenerationException, JsonMappingException, IOException {
		UserDAO userDao = (UserDAO) ctx.getAttribute("userDAO");
		String username1 = values.get("username1");
		String username2 = values.get("username2");
		
		userDao.unfriend(username1, username2);
		userDao.saveChanges();
		
		User u2 = userDao.getByUsername(username2);
		
		return u2;
	}
	
	@POST
	@Path("/respondToRequest")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public User respondToRequest(Map<String, String> values) throws ParseException, JsonGenerationException, JsonMappingException, IOException {
		UserDAO userDao = (UserDAO) ctx.getAttribute("userDAO");
		RequestDAO requestDao = (RequestDAO) ctx.getAttribute("requestDAO");
		
		int id = Integer.parseInt(values.get("requestId"));
		boolean accepted = Boolean.parseBoolean(values.get("accepted"));
		FriendRequest r = requestDao.getRequestById(id);
		
		if(accepted) {
			r.setState(FriendRequestState.ACCEPTED);
			userDao.makeFriends(r.getSender(), r.getRecipient());
			
		}else {
			r.setState(FriendRequestState.DECLINED);
		}
		User u = userDao.getByUsername(r.getRecipient());
		requestDao.saveChanges();
		userDao.saveChanges();
		
		return u;
	}
	
	@GET
	@Path("/getRequests")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<FriendRequest> getFriendRequests(@QueryParam("username") String username) {
		RequestDAO requestDao = (RequestDAO) ctx.getAttribute("requestDAO");
		return requestDao.getUsersRequests(username);
		
	}
	
	@GET
	@Path("/getFriends")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<User> getFriends(@QueryParam("username") String username) {
		UserDAO userDao = (UserDAO) ctx.getAttribute("userDAO");
		Collection<User> retList = new ArrayList<>();
		for(String usr : userDao.getByUsername(username).getFriends()) {
			retList.add(userDao.getByUsername(usr));
		}
		
		return retList;
	}
	
	@POST
	@Path("/changeProfilePicture")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public User changeProfilePicture(Map<String, String> values) throws ParseException, JsonGenerationException, JsonMappingException, IOException {
		UserDAO userDao = (UserDAO) ctx.getAttribute("userDAO");
		String pictureId = values.get("pictureId");
		String username = values.get("username");
		PictureDAO pictureDao = (PictureDAO)ctx.getAttribute("pictureDAO");
		Picture p = pictureDao.getPictureById(Integer.parseInt(pictureId));
		
		userDao.saveChanges();
		
		return userDao.changeProfilePicture(username, p.getPicture());
	}
	
	
}
