package services;

import java.io.IOException;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSessionContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import beans.Gender;
import beans.Role;
import beans.User;
import dao.PictureDAO;
import dao.PostDAO;
import dao.RequestDAO;
import dao.UserDAO;

@Path("/registration")
public class RegistrationService {
	
	@Context
	ServletContext ctx;
	
	public RegistrationService() {}
	
	@PostConstruct
	public void init() {
		if (ctx.getAttribute("userDAO") == null) {
			String pathForPosts = ctx.getRealPath("/data/posts.json");
			ctx.setAttribute("postDAO", new PostDAO(pathForPosts));
			
			String pathForRequests = ctx.getRealPath("/data/requests.json");
			ctx.setAttribute("requestDAO", new RequestDAO(pathForRequests));
			
			String pathForPictures = ctx.getRealPath("/data/pictures.json");
			ctx.setAttribute("pictureDAO", new PictureDAO(pathForPictures));
			
	    	String contextPath = ctx.getRealPath("/data/users.json");
			ctx.setAttribute("userDAO", new UserDAO(contextPath, (PostDAO)ctx.getAttribute("postDAO"), (RequestDAO)ctx.getAttribute("requestDAO"), (PictureDAO)ctx.getAttribute("pictureDAO")));
		}
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response register(HashMap<String, String> values) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		
		UserDAO userDao = (UserDAO) ctx.getAttribute("userDAO");
		User userExists = userDao.getByUsername(values.get("username"));
		if (userExists != null) {
			return Response.status(400).entity("User already exists").build();
		}
		User newuser = new User(values.get("username"), values.get("password"), values.get("email"), values.get("name"), values.get("lastName"), values.get("dateOfBirth"), Gender.valueOf(values.get("gender")), Role.USER, "", false);
		userDao.addUser(newuser);
		String jsonString = mapper.writeValueAsString(newuser);
		userDao.saveChanges();
		
		return Response.ok(jsonString, MediaType.APPLICATION_JSON).build();
	}
}
