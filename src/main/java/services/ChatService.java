package services;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;

import dao.MessagesDAO;
import dao.PostDAO;
import dao.RequestDAO;
import dao.UserDAO;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.util.Timer;
import java.util.TimerTask;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@ServerEndpoint("/chat")
public class ChatService {
	@Context
	ServletContext ctx;
	
	static Logger log = Logger.getLogger("Websockets endpoint");
	
	static List<Session> sessions = new ArrayList<Session>();
	Timer t = new Timer();
	
	public ChatService() {}
	
	@OnMessage
	public void echoTextMessage(Session session, String msg, boolean last) {
		try {
			if (session.isOpen()) {
				System.out.println("Usao u echo");
				log.info("Websocket endpoint: " + this.hashCode() + " primio: " + msg + " u sesiji: " + session.getId());
                for (Session s : sessions) {
                	System.out.println("Usao u for");
                    if (!s.getId().equals(session.getId())) {
                    	System.out.println("Poruka ===="+msg);
                        s.getBasicRemote().sendText(msg, last);
                    }
                }
			}
		} catch (IOException e) { try { session.close(); } catch (IOException e1) {}}
	}
	
	
	@OnOpen
	public void onOpen(Session session) {
	  if (!sessions.contains(session)) {
	    sessions.add(session);
	    log.info("Dodao sesiju: " + session.getId());
	  }
	}
	@OnClose
	public void onClose(Session session) {
	  sessions.remove(session);
	  log.info("Zatvorio: " + session.getId() + " u endpoint-u: " + this.hashCode());
	}

	@OnError
    public void error(Session session, Throwable t) {
        sessions.remove(session);
        log.log(Level.SEVERE, "Gre�ka u sessiji: " + session.getId() + " u endpoint-u: " + this.hashCode() + ", tekst: " + t.getMessage());
        //t.printStackTrace();
    }
	
}
