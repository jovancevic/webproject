package services;

import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import beans.User;
import dao.PictureDAO;
import dao.PostDAO;
import dao.RequestDAO;
import dao.UserDAO;

@Path("/log")
public class LoginService {
	
	@Context
	ServletContext ctx;
	
	public LoginService() {}
	
	@PostConstruct
	public void init() {
		if (ctx.getAttribute("userDAO") == null) {
			String pathForPosts = ctx.getRealPath("/data/posts.json");
			ctx.setAttribute("postDAO", new PostDAO(pathForPosts));
			
			String pathForRequests = ctx.getRealPath("/data/requests.json");
			ctx.setAttribute("requestDAO", new RequestDAO(pathForRequests));
			
			String pathForPictures = ctx.getRealPath("/data/pictures.json");
			ctx.setAttribute("pictureDAO", new PictureDAO(pathForPictures));
			
	    	String contextPath = ctx.getRealPath("/data/users.json");
			ctx.setAttribute("userDAO", new UserDAO(contextPath, (PostDAO)ctx.getAttribute("postDAO"), (RequestDAO)ctx.getAttribute("requestDAO"), (PictureDAO)ctx.getAttribute("pictureDAO")));
		}
	}
	
	@POST
	@Path("/in")
	@Consumes(MediaType.APPLICATION_JSON)
	public User login(HashMap<String, String> values) {
		UserDAO userDao = (UserDAO) ctx.getAttribute("userDAO");
		User loggedUser = userDao.find(values.get("username"), values.get("password"));

		return loggedUser;
	}
}
