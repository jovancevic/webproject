package services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSessionContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import beans.Comment;
import beans.Gender;
import beans.Picture;
import beans.Role;
import beans.User;
import beans.Post;
import dao.PictureDAO;
import dao.PostDAO;
import dao.RequestDAO;
import dao.UserDAO;

@Path("/feed")
public class PostService {
	
	@Context
	ServletContext ctx;
	
	static HashMap<String, Integer> photoIds = new HashMap<>();
	
	
	public PostService() {}
	
	@PostConstruct
	public void init() {
		if (ctx.getAttribute("userDAO") == null) {
			String pathForPosts = ctx.getRealPath("/data/posts.json");
			ctx.setAttribute("postDAO", new PostDAO(pathForPosts));
			
			String pathForRequests = ctx.getRealPath("/data/requests.json");
			ctx.setAttribute("requestDAO", new RequestDAO(pathForRequests));
			
			String pathForPictures = ctx.getRealPath("/data/pictures.json");
			ctx.setAttribute("pictureDAO", new PictureDAO(pathForPictures));
			
	    	String contextPath = ctx.getRealPath("/data/users.json");
			ctx.setAttribute("userDAO", new UserDAO(contextPath, (PostDAO)ctx.getAttribute("postDAO"), (RequestDAO)ctx.getAttribute("requestDAO"), (PictureDAO)ctx.getAttribute("pictureDAO")));
		}
	}
	
	@GET
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFriendsPosts(@QueryParam("username") String username) throws JsonProcessingException {
		if (username == null) {
			return Response.status(403).entity("Nemate pravo pristupa.").build();
		}
		ObjectMapper mapper = new ObjectMapper();
		UserDAO userDao = (UserDAO) ctx.getAttribute("userDAO");
		List<User> friends = new ArrayList<User>();
		
		for (String friend : userDao.getByUsername(username).getFriends()) {
			friends.add(userDao.getByUsername(friend));
		}
		String jsonString = mapper.writeValueAsString(friends);
		
		return Response.ok(jsonString, MediaType.APPLICATION_JSON).build();
	}
	
	@DELETE
	@Path("/deletePost/{id}")
	public User deletePostById(@PathParam("id") String id) throws IOException {
		UserDAO userDao = (UserDAO) ctx.getAttribute("userDAO");
		PostDAO postDao = (PostDAO)ctx.getAttribute("postDAO");
		postDao.removePost(Integer.parseInt(id));
		postDao.saveChanges();
		User u = userDao.deletePostById(Integer.parseInt(id));
		System.out.println(u);
		userDao.saveChanges();
		
		return u;
	}
	
	@DELETE
	@Path("/deleteCommentPost/{postId}/{commentId}")
	public Post deleteCommentById(@PathParam("postId") String postId, @PathParam("commentId") String commentId) throws JsonGenerationException, JsonMappingException, IOException {
		PostDAO postDao = (PostDAO)ctx.getAttribute("postDAO");
		UserDAO userDao = (UserDAO) ctx.getAttribute("userDAO");
		
		Post p = postDao.removeCommentById(Integer.parseInt(postId), Integer.parseInt(commentId));
		User u = userDao.getByUsername(p.getUsername());
		u.removePostComment(Integer.parseInt(postId), Integer.parseInt(commentId));
		postDao.saveChanges();
		userDao.saveChanges();
		
		return p;
	}
	
	
	@POST
	@Path("/uploadPicture")
	public Picture uploadPicture(HashMap<String, String> values) throws FileNotFoundException, IOException {
		String[] tokens = values.get("data").split("&");
		PictureDAO pictureDao = (PictureDAO) ctx.getAttribute("pictureDAO");
		UserDAO userDAO = (UserDAO) ctx.getAttribute("userDAO");
		
		String UPLOAD_FOLDER = ctx.getRealPath("/photos/");
		
		String username = tokens[0];
		String encodeImg = tokens[2];
		
		byte[] decoded = Base64.getDecoder().decode(encodeImg);
		if(photoIds.containsKey(username)) {
			photoIds.put(username, photoIds.get(username) + 1);
		}
		else {
			int num = userDAO.numberOfUserPhotos(username);
			photoIds.put(username, num+1);
		}
		File theDir = new File(UPLOAD_FOLDER);
		if (!theDir.exists()){
		    theDir.mkdirs();
		}
		
		try (OutputStream stream = new FileOutputStream(UPLOAD_FOLDER + username + photoIds.get(username) + ".jpg")){
			System.out.println(UPLOAD_FOLDER + username + photoIds.get(username) + ".jpg");
			stream.write(decoded);
			stream.flush();
			Picture pic = pictureDao.addPicture(tokens[0], "photos/" + username + photoIds.get(username) + ".jpg", tokens[1]);
			userDAO.addPicture(username,pic);
			pictureDao.saveChanges();
			userDAO.saveChanges();
			
			return pic;
			
		}catch(Exception ex) {
			System.out.println(ex);
		}
		return null;
		
	}
	
	@POST
	@Path("/uploadPost")
	@Produces(MediaType.APPLICATION_JSON)
	public Post uploadPost(HashMap<String, String> values) throws FileNotFoundException, IOException {
		PostDAO postDao = (PostDAO) ctx.getAttribute("postDAO");
		UserDAO userDAO = (UserDAO) ctx.getAttribute("userDAO");
		
		String UPLOAD_FOLDER = ctx.getRealPath("/photos/");
		
		String[] tokens = values.get("data").split("&");
		String username = tokens[0];
		
		if(tokens.length == 2)
		{
			Post post = postDao.addPost(tokens[0], tokens[1], "");
			userDAO.addPost(username, post);
			postDao.saveChanges();
			userDAO.saveChanges();
			
			return post;
		}
		
		String encodeImg = tokens[2];
		byte[] decoded = Base64.getDecoder().decode(encodeImg);
		if(photoIds.containsKey(username)) {
			photoIds.put(username, photoIds.get(username) + 1);
		}
		else {
			int num = userDAO.numberOfUserPhotos(username);
			photoIds.put(username, num+1);
		}
		File theDir = new File(UPLOAD_FOLDER);
		if (!theDir.exists()){
		    theDir.mkdirs();
		}
		
		try (OutputStream stream = new FileOutputStream(UPLOAD_FOLDER + username + photoIds.get(username) + "_post" + ".jpg")){
			System.out.println(UPLOAD_FOLDER + username + photoIds.get(username) + ".jpg");
			stream.write(decoded);
			Post post = postDao.addPost(tokens[0], "photos/" + username + photoIds.get(username) + "_post" + ".jpg", tokens[1]);
			userDAO.addPost(username,post);
			postDao.saveChanges();
			userDAO.saveChanges();
			
			return post;
			
		}catch(Exception ex) {
			System.out.println(ex);
		}
		return null;
		
	}
	
	@POST
	@Path("/commentPost")
	@Produces(MediaType.APPLICATION_JSON)
	public Post commentPost(HashMap<String, String> values) throws JsonGenerationException, JsonMappingException, IOException {
		int postId = Integer.parseInt(values.get("post"));
		UserDAO userDAO = (UserDAO) ctx.getAttribute("userDAO");
		String username = values.get("username");
		String text = values.get("comment");
		PostDAO postDao = (PostDAO)ctx.getAttribute("postDAO");
		Post p = postDao.getPostById(postId);
		
		p.addComment(new Comment(username, LocalDate.now(), text));
		User u = userDAO.getByUsername(p.getUsername());
		u.changePost(p.getId(), p);
		
		postDao.saveChanges();
		userDAO.saveChanges();
		
		return p;
	}
	
	@POST
	@Path("/commentPicture")
	@Produces(MediaType.APPLICATION_JSON)
	public Picture commentPicture(HashMap<String, String> values) throws JsonGenerationException, JsonMappingException, IOException {
		int pictureId = Integer.parseInt(values.get("picture"));
		UserDAO userDAO = (UserDAO) ctx.getAttribute("userDAO");
		String username = values.get("username");
		String text = values.get("comment");
		PictureDAO pictureDao = (PictureDAO)ctx.getAttribute("pictureDAO");
		Picture p = pictureDao.getPictureById(pictureId);
		
		p.addComment(new Comment(username, LocalDate.now(), text));
		User u = userDAO.getByUsername(p.getUsername());
		u.changePicture(p.getId(), p);
		pictureDao.saveChanges();
		userDAO.saveChanges();
		
		return p;
	}
	
	@DELETE
	@Path("/deletePicture/{id}")
	public User deletePictureById(@PathParam("id") String id) throws JsonGenerationException, JsonMappingException, IOException {
		UserDAO userDao = (UserDAO) ctx.getAttribute("userDAO");
		PictureDAO pictureDao = (PictureDAO)ctx.getAttribute("pictureDAO");
		pictureDao.removePicture(Integer.parseInt(id));
		pictureDao.saveChanges();
				
		return userDao.deletePictureById(Integer.parseInt(id));
	}
	
	@GET
	@Path("/getPicture")
	public Picture getPicture(@QueryParam("pictureId") String id) {
		
		PictureDAO pictureDao = (PictureDAO)ctx.getAttribute("pictureDAO");
		
		return pictureDao.getPictureById(Integer.parseInt(id));
	}
	
	@GET
	@Path("/getPost")
	public Post getPost(@QueryParam("postId") String id) {
		
		PostDAO postDao = (PostDAO)ctx.getAttribute("postDAO");
		return postDao.getPostById(Integer.parseInt(id));
	}
	
	@DELETE
	@Path("/deleteCommentPicture/{pictureId}/{commentId}")
	public Picture deleteCommentPictureById(@PathParam("pictureId") String pictureId, @PathParam("commentId") String commentId) throws JsonGenerationException, JsonMappingException, IOException {
		PictureDAO pictureDao = (PictureDAO)ctx.getAttribute("pictureDAO");
		UserDAO userDao = (UserDAO) ctx.getAttribute("userDAO");
		Picture p = pictureDao.removeCommentById(Integer.parseInt(pictureId), Integer.parseInt(commentId));
		
		User u = userDao.getByUsername(p.getUsername());
		u.removePictureComment(Integer.parseInt(pictureId), Integer.parseInt(commentId));
		pictureDao.saveChanges();
		
		return p;
	}
	
}
