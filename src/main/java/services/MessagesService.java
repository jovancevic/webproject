package services;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

import beans.DirectMessage;
import beans.User;
import dao.MessagesDAO;
import dao.UserDAO;

@Path("/dm")
public class MessagesService {

	@Context
	ServletContext ctx;

	public MessagesService() {
	}

	@PostConstruct
	public void init() {
		if (ctx.getAttribute("messagesDAO") == null) {
			String contextPath = ctx.getRealPath("/data/messages.json");
			ctx.setAttribute("messagesDAO", new MessagesDAO(contextPath));
		}
	}

//	@GET
//	@Path("/")
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response login() {
//		return Response.status(200).build();
//	}
//	

	@GET
	@Path("/getMessages")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<DirectMessage> getMessages(@QueryParam("username1") String username1,
			@QueryParam("username2") String username2) {
		MessagesDAO messageDAO = (MessagesDAO) ctx.getAttribute("messagesDAO");
		if (messageDAO == null) {
			return null;
		}
		return messageDAO.getMessagesBetweenUsers(username1, username2);
	}

	@GET
	@Path("/getMessagedUsers")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<User> getMessagedUsers(@QueryParam("loggedUser") String loggedUser) {
		MessagesDAO messageDAO = (MessagesDAO) ctx.getAttribute("messagesDAO");
		if (messageDAO == null) {
			return null;
		}
		UserDAO userDAO = (UserDAO) ctx.getAttribute("userDAO");

		if (userDAO == null) {
			System.out.println("Ne postoji userDAO");
			return null;
		}
		List<String> usersUsernames = messageDAO.getMessagedUsers(loggedUser);

		List<User> users = new ArrayList<>();
		for (String username : usersUsernames) {
			users.add(userDAO.getByUsername(username));
		}

		return users;
	}

	@POST
	@Path("/sendMessage")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<DirectMessage> sendMessage(Map<String, String> values)
			throws JsonGenerationException, JsonMappingException, IOException {
		MessagesDAO messageDAO = (MessagesDAO) ctx.getAttribute("messagesDAO");

		if (messageDAO == null) {
			return null;
		}
		DirectMessage m = new DirectMessage(values.get("fromUser"), values.get("toUser"), values.get("content"),
				LocalDateTime.now());
		messageDAO.addMessage(m);
		messageDAO.saveChanges();
		
		return messageDAO.getMessagesBetweenUsers(values.get("fromUser"), values.get("toUser"));
	}

}
